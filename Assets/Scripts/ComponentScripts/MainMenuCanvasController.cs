using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvasController : MonoBehaviour
{
    private Canvas popupCanvas;
    void Awake()
    {
        popupCanvas = Resources.Load<Canvas>("UIPrefabs/PopupCanvas");
    }

    public void ShowOptionsDialog ()
    {
        var obj = Instantiate(popupCanvas, transform.parent);
        obj.GetComponent<PopupCanvasController>().SetHeaderText("Under Construction");
        obj.GetComponent<PopupCanvasController>().SetBodyText("This is part of a work-in-progress feature.\nCome back when this feature is ready.");
        obj.GetComponent<PopupCanvasController>().SetButtonText("OK");
    }

}
