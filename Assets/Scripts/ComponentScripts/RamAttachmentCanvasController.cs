using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RamAttachmentCanvasController : MonoBehaviour
{
    public GameObject sourceCanvas;
    public GameObject destCanvas;
    private GameObject gameManager;
    public InventoryPart selectedPart;

    private void Awake()
    {
        sourceCanvas = transform.gameObject;
        gameManager = GameObject.Find("GameManager");

        GameObject[] objects = FindObjectsOfType<GameObject>();
        foreach (GameObject gameObject in objects)
        {
            if (gameObject.GetComponent<CanvasGroup>())
            {
                destCanvas = gameObject;
            }
        }
        destCanvas.GetComponent<CanvasGroup>().interactable = false;
    }

    public void CloseCanvas()
    {
        destCanvas.GetComponent<CanvasGroup>().interactable = true;
        Destroy(sourceCanvas);
    }

    public void AttachDimm1()
    {
        gameManager.GetComponent<GameManagerController>().AttachRam(selectedPart, "dimm1");
    }

    public void AttachDimm2()
    {
        gameManager.GetComponent<GameManagerController>().AttachRam(selectedPart, "dimm2");
    }

    public void AttachDimm3()
    {
        gameManager.GetComponent<GameManagerController>().AttachRam(selectedPart, "dimm3");
    }

    public void AttachDimm4()
    {
        gameManager.GetComponent<GameManagerController>().AttachRam(selectedPart, "dimm4");
    }
}
