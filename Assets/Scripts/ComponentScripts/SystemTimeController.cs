using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SystemTimeController : MonoBehaviour
{
    private void Update()
    {
        GetComponent<TextMeshProUGUI>().text = System.DateTime.Now.ToString("HH:mm");
    }
}
