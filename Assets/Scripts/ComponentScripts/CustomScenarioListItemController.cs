using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CustomScenarioListItemController : MonoBehaviour
{
    public string scenarioFileName;

    public void LoadCustomScene ()
    {
        GlobalPlayerData.Instance.sceneToLoad = "CustomScenarios/" + scenarioFileName;
        SceneManager.LoadScene("TestScene");
    }

    public void LoadStandardScene ()
    {
        GlobalPlayerData.Instance.sceneToLoad = "StandardScenarios/" + scenarioFileName;
        SceneManager.LoadScene("TestScene");
    }
}
