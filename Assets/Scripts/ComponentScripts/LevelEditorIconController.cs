using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEditorIconController : MonoBehaviour
{
    private GameObject DataManager;
    public InventoryPart inventoryPart;
    private void Awake()
    {
        DataManager = GameObject.Find("DataManager");
    }

    public void ChoosePart()
    {
        DataManager.GetComponent<DataManagerController>().SelectPart(inventoryPart);
    }

}
