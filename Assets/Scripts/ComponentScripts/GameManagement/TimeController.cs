using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeController : MonoBehaviour
{
    public float delay = 1f;
    public TextMeshProUGUI text;
    public int minutes = 0;
    public float seconds = 0;
    private void Update()
    {
        if (seconds > 60.0f)
        {
            minutes += 1;
            seconds = 0f;
        } else
        {
            seconds += Time.deltaTime;
        }

        if (seconds < 10)
        {
            if (minutes < 10)
                text.text = "Elapsed Time: 0" + minutes.ToString() + ":0" + System.Math.Round(seconds, 1).ToString();
            else
                text.text = "Elapsed Time: " + minutes.ToString() + ":0" + System.Math.Round(seconds, 1).ToString();
        } else
        {
            if (minutes < 10)
                text.text = "Elapsed Time: 0" + minutes.ToString() + ":" + System.Math.Round(seconds, 1).ToString();
            else
                text.text = "Elapsed Time: " + minutes.ToString() + ":" + System.Math.Round(seconds, 1).ToString();
        }
    }
}
