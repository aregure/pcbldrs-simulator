using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GpuAttachmentCanvasController : MonoBehaviour
{
    public GameObject sourceCanvas;
    public GameObject destCanvas;
    private GameObject gameManager;
    public InventoryPart selectedPart;

    private void Awake()
    {
        sourceCanvas = transform.gameObject;
        gameManager = GameObject.Find("GameManager");

        GameObject[] objects = FindObjectsOfType<GameObject>();
        foreach (GameObject gameObject in objects)
        {
            if (gameObject.GetComponent<CanvasGroup>())
            {
                destCanvas = gameObject;
            }
        }
        destCanvas.GetComponent<CanvasGroup>().interactable = false;
    }

    public void CloseCanvas()
    {
        destCanvas.GetComponent<CanvasGroup>().interactable = true;
        Destroy(sourceCanvas);
    }

    public void AttachTop ()
    {
        gameManager.GetComponent<GameManagerController>().AttachGpu(selectedPart, "top");
    }

    public void AttachBottom ()
    {
        gameManager.GetComponent<GameManagerController>().AttachGpu(selectedPart, "bottom");
    }

}
