using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadCanvas : MonoBehaviour
{
    public GameObject sourceCanvas;
    public GameObject destCanvas;
    public void OpenCanvas()
    {
        if (destCanvas != null)
        {
            sourceCanvas.SetActive(false);
            destCanvas.SetActive(true);
        }
    }

    public void OpenCanvasOverlay()
    {
        GameObject[] gameObjects = FindObjectsOfType<GameObject>();
        destCanvas.SetActive(true);

        foreach (GameObject gameObject in gameObjects)
        {
            if (gameObject != destCanvas && gameObject.GetComponent<CanvasGroup>())
            {
                gameObject.GetComponent<CanvasGroup>().interactable = false;
            }
        }

    }

    public void CloseCanvasOverlay()
    {
        if (destCanvas != null)
        {
            GameObject[] gameObjects = FindObjectsOfType<GameObject>();
            sourceCanvas.SetActive(false);

            foreach (GameObject gameObject in gameObjects)
            {
                if (gameObject != sourceCanvas && gameObject.GetComponent<CanvasGroup>())
                {
                    gameObject.GetComponent<CanvasGroup>().interactable = true;
                }
            }
        }
    }

    public void ToggleCanvasOverlay()
    {
        if (destCanvas != null)
        {
            if (destCanvas.activeInHierarchy)
            {
                destCanvas.SetActive(false);
            }
            else
            {
                destCanvas.SetActive(true);
            }
        }
    }

}
