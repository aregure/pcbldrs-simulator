using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ObjectivesPanelController : MonoBehaviour
{
    private GameManagerController gameManager;
    public GameObject objectiveListContainer;

    private void Awake()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManagerController>();
        UpdateObjectiveDisplay();
    }

    public void ToggleObjectivesPanel ()
    {
        print("panel toggled");
        if (GetComponent<RectTransform>().anchoredPosition == new Vector2(-270f, 300f))
        {
            GetComponent<RectTransform>().anchoredPosition = new Vector2(-270f, -210f);
            GetComponent<Image>().color = new Color(0, 0, 0, 0);
            transform.GetChild(1).gameObject.SetActive(false);
        }
        else
        {
            GetComponent<RectTransform>().anchoredPosition = new Vector2(-270f, 300f);
            GetComponent<Image>().color = new Color(0, 0, 0, 0.5490f);
            transform.GetChild(1).gameObject.SetActive(true);
        }

    }

    public void UpdateObjectiveDisplay ()
    {
        if (gameManager.currentScenario.isTutorialScenario)
        {
            if (objectiveListContainer.transform.childCount > 0)
            {
                for (int i = 0; i < objectiveListContainer.transform.childCount; i++)
                {
                    Destroy(objectiveListContainer.transform.GetChild(i).gameObject);
                }
            }

            foreach (Objective objective in gameManager.currentScenario.scenarioObjectives)
            {
                var obj = Instantiate(Resources.Load<GameObject>("UIPrefabs/ObjectiveItem"), objectiveListContainer.transform);
                obj.transform.GetChild(0).GetComponent<Toggle>().isOn = objective.isSatisfied;
                string objectiveText = objective.objectiveType switch
                {
                    GameEnums.ObjectiveType.makeBootable => "Make system bootable",
                    // GameEnums.ObjectiveType.noPartsBroken => "No parts broken",
                    GameEnums.ObjectiveType.timeLimit => $"Time limit {objective.objectiveTimeLimit} minutes",
                    GameEnums.ObjectiveType.reconfigureSystem => objective.configType switch
                    {
                        // GameEnums.SystemConfigurationType.installOs => "Install an operating system",
                        GameEnums.SystemConfigurationType.memoryProfile => "Adjust memory profile",
                        GameEnums.SystemConfigurationType.systemTempRatio => "Fix overheating issues",
                        _ => "Unknown configuration type",
                    },
                    _ => "Unknown objective"

                } + (objective.isMainObjective ? " [Main objective]" : "");
                obj.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = objectiveText;
            }
        }
    }
}
