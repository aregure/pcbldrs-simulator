using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
[AddComponentMenu("UI/Animate Canvas")]
public class AnimateCanvas : Image
{
    public bool animate;
    public float time = 0.5f;
    public float delay = 0f;

    new CanvasGroup canvas;

    protected override void Awake()
    {
        canvas = GetComponent<CanvasGroup>();
    }

    protected override void OnEnable()
    {
        if (Application.isPlaying)
        {
            material.SetFloat("_Size", 0);
            canvas.alpha = 0;
            LeanTween.value(gameObject, UpdateAlphaFade, 0, 1, time).setDelay(delay);
        }
    }

    void UpdateAlphaFade(float value)
    {
        material.SetFloat("_Size", value);
        canvas.alpha = value;
    }

}
