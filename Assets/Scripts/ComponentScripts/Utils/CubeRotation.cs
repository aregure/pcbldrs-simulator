using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeRotation : MonoBehaviour
{
    [SerializeField] [Range(0.1f, 5f)] private float rotationSpeed;
    private void Start()
    {
        rotationSpeed = 1f;
    }

    private void Update()
    {
        transform.Rotate(new Vector3 (0, 10 * rotationSpeed, 0) * Time.deltaTime);
    }
}
