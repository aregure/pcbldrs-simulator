using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FanBladeRotation : MonoBehaviour
{
    [SerializeField] [Range(1f, 10f)] private float fanSpeed = 1.0f;
    void Update()
    {
        transform.Rotate(new Vector3(0, 0, fanSpeed * 250) * Time.deltaTime);
    }
}
