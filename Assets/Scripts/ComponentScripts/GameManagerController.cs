using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using Newtonsoft.Json.Linq;
using System.Linq;
using TMPro;
using UnityEngine.UI;

public class GameManagerController : MonoBehaviour
{
    public InventoryDisplay inventoryDisplay;
    public List<InventoryPart> scenarioItems;
    public Scenario currentScenario;

    public GameObject ScreenViewCanvas;
    public GameObject StressTestCanvas;
    public GameObject BIOSCanvas;
    public GameObject BootCanvas;
    public GameObject BlueScreenCanvas;

    public GameObject SuccessCanvas;
    public GameObject FailCanvas;

    public GameObject memoryProfileDropdownObject;
    public GameObject cpuPowerLimitDropdownObject;
    private TMP_Dropdown memoryProfileDropdown;
    private TMP_Dropdown cpuPowerLimitDropdown;

    public GameObject monitor;
    public GameObject keyboard;
    public GameObject mouse;

    public GameObject cpuInfo;
    public GameObject memInfo;
    public GameObject systemDate;
    public GameObject systemTime;

    public string selectedPartId;
    public string attachmentPosition;

    public bool selectionModeEnabled;

    public bool scenarioCleared;

    public ObjectivesPanelController objectivesPanelController;

    [SerializeField] private int currentTimeMinutes;
    [SerializeField] private float currentTimeSeconds;

    [SerializeField] private int finalTimeMinutes;
    [SerializeField] private int finalTimeSeconds;

    public bool isSystemOverheating;
    public int systemTemperature;
    public int cpuPowerLimit;
    public int cpuTdp;

    // public bool isOsInstalled;

    public bool isMemoryProfileChanged;
    public int memorySpeed;

    public bool partBroken;
    public bool testModeEnabled;
    public bool isBiosTriggerable;
    public bool isBiosTriggered;

    public int timeLimit;

    private void Awake()
    {
        // parse Scenario object
        LoadScenario();

        currentTimeMinutes = 0;
        currentTimeSeconds = 0f;

        selectionModeEnabled = false;
        scenarioCleared = false;

        memoryProfileDropdown = memoryProfileDropdownObject.GetComponent<TMP_Dropdown>();
        cpuPowerLimitDropdown = cpuPowerLimitDropdownObject.GetComponent<TMP_Dropdown>();

        cpuPowerLimit = 2;

        memorySpeed = 2666;

        foreach (Objective objective in currentScenario.scenarioObjectives) {
            if (objective.objectiveType == GameEnums.ObjectiveType.timeLimit)
            {
                timeLimit = objective.objectiveTimeLimit;
                objective.isSatisfied = true;
            //} else if (objective.objectiveType == GameEnums.ObjectiveType.noPartsBroken)
            //{
            //    partBroken = false;
            } else if (objective.objectiveType == GameEnums.ObjectiveType.reconfigureSystem)
            {
                //if (objective.configType == GameEnums.SystemConfigurationType.installOs)
                //{
                //    isOsInstalled = false;
                //} else
                if (objective.configType == GameEnums.SystemConfigurationType.memoryProfile)
                {
                    isMemoryProfileChanged = false;
                } else if (objective.configType == GameEnums.SystemConfigurationType.systemTempRatio)
                {
                    isSystemOverheating = true;
                }
            }
        }

        if (!currentScenario.isTutorialScenario)
        {
            GameObject.Find("ObjectivesCanvas").SetActive(false);
        }

        // generate scenario items
        GenerateInventory();

        // instantiate non-stowed objects to world space
        SetupWorld();

        // display objectives (if tutorial scenario)

    }

    private void Update()
    {
        if (currentTimeSeconds > 60.0f)
        {
            currentTimeMinutes += 1;
            currentTimeSeconds = 0f;
        }
        else
        {
            currentTimeSeconds += Time.deltaTime;
        }

        if (isBiosTriggerable)
        {
            if (Input.GetKeyDown(KeyCode.Delete))
            {
                isBiosTriggered = true;
                print("BIOS triggered!");
            }
        }

        systemDate.GetComponent<TextMeshProUGUI>().text = System.DateTime.Now.ToString("dddd, dd MMMM yyyy");
        systemTime.GetComponent<TextMeshProUGUI>().text = System.DateTime.Now.ToString("HH:mm:ss");

        if (timeLimit != 0)
        {
            if (currentTimeMinutes >= timeLimit)
            {
                print("simulation failed");
                FailSimulation();
                timeLimit = 0;
            }
        }

    }

    public void SetupWorld()
    {
        // for each non-stowed inventory part:
        //     Instantiate prefab
        //     set prefab name to InventoryPart's partId to make it unique
        //     set transform according to Transforms.cs
        //         if part is attached to another part:
        //             set transforms first, then set parent

        // spawn order: motherboard,
        //              parts that attach to motherboard except gpu,
        //              case,
        //              parts that attach to case,
        //              gpu

        // spawn motherboard
        Debug.Log("Spawning items");
        InventoryPartMotherboard
            moboToSpawn = scenarioItems.Find(item => !item.isStowed && item.GetType() == typeof(InventoryPartMotherboard))
            as InventoryPartMotherboard;

        if (moboToSpawn != null)
        {
            SpawnPart(moboToSpawn.partId, Transforms.pos_moboOnDesk, Transforms.rot_moboOnDesk, null);
            GameObject spawnedMobo = GameObject.Find($"{moboToSpawn.partId}");

            // spawn parts that attach to motherboard
            List<InventoryPart> nonStowedMoboAttachments = new();
            foreach (InventoryPart attachment in scenarioItems)
            {
                if (!attachment.isStowed)
                {
                    if (attachment.partType == InventoryPartType.CPU)
                        SpawnPart(attachment.partId, Transforms.pos_cpuMountedOnMobo, Transforms.rot_cpuMountedOnMobo, spawnedMobo);
                    else if (attachment.partType == InventoryPartType.CPUCooler)
                        SpawnPart(attachment.partId, Transforms.pos_cpuCoolerMountedOnMobo, Transforms.rot_cpuCoolerOnMobo, spawnedMobo);
                    else if (attachment.partType == InventoryPartType.RAM)
                    {
                        Port targetPort = moboToSpawn.ports.Find(port => port.occupiedByPartId == attachment.partId);
                        if (targetPort.portName == "DIMM Slot 1")
                            SpawnPart(attachment.partId, Transforms.pos_dimm1MountedOnMobo, Transforms.rot_dimmOnMobo, spawnedMobo);
                        else if (targetPort.portName == "DIMM Slot 2")
                            SpawnPart(attachment.partId, Transforms.pos_dimm2MountedOnMobo, Transforms.rot_dimmOnMobo, spawnedMobo);
                        else if (targetPort.portName == "DIMM Slot 3")
                            SpawnPart(attachment.partId, Transforms.pos_dimm3MountedOnMobo, Transforms.rot_dimmOnMobo, spawnedMobo);
                        else if (targetPort.portName == "DIMM Slot 4")
                            SpawnPart(attachment.partId, Transforms.pos_dimm4MountedOnMobo, Transforms.rot_dimmOnMobo, spawnedMobo);
                    } else if (attachment.partType == InventoryPartType.SSDM2)
                        SpawnPart(attachment.partId, Transforms.pos_m2MountedOnMobo, Transforms.rot_m2OnMobo, spawnedMobo);
                }
            }

            // spawn case
            InventoryPartCase
                caseToSpawn = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed)
                as InventoryPartCase;

            if (caseToSpawn != null)
            {
                SpawnPart(caseToSpawn.partId, Transforms.pos_caseLaidOnDesk, Transforms.rot_caseLaidOnDesk, null);
                GameObject spawnedCase = GameObject.Find($"{caseToSpawn.partId}");

                // attach IO shield
                var ioShield = Instantiate(Resources.Load<GameObject>("Prefabs/ioShield"), null);
                ioShield.transform.position = Transforms.pos_ioShieldOnCase;
                ioShield.transform.localEulerAngles = Transforms.rot_ioShieldOnCase;
                ioShield.transform.parent = spawnedCase.transform;

                // attach motherboard cluster to case
                spawnedMobo.transform.parent = null;
                spawnedMobo.transform.position = Transforms.pos_moboMountedOnCase;
                spawnedMobo.transform.parent = spawnedCase.transform;

                // spawn other parts that attach to case when laid
                foreach (InventoryPart attachment in scenarioItems)
                {
                    if (!attachment.isStowed)
                    {
                        if (attachment.partType == InventoryPartType.GPU)
                        {
                            Port targetPort = moboToSpawn.ports.Find(port => port.occupiedByPartId == attachment.partId);
                            if (targetPort.portName == "PCIe Slot (Top)")
                            {
                                SpawnPart(attachment.partId, Transforms.pos_gpuMountedOnMoboTop, Transforms.rot_gpuOnMobo, spawnedCase);
                                spawnedCase.transform.GetChild(4).gameObject.SetActive(false);
                                spawnedCase.transform.GetChild(5).gameObject.SetActive(false);
                            } else if (targetPort.portName == "PCIe Slot (Bottom)")
                            {
                                spawnedCase.transform.GetChild(1).gameObject.SetActive(false);
                                spawnedCase.transform.GetChild(2).gameObject.SetActive(false);
                                SpawnPart(attachment.partId, Transforms.pos_gpuMountedOnMoboBottom, Transforms.rot_gpuOnMobo, spawnedCase);
                            }
                        }
                    }
                }

                // make case upright
                spawnedCase.transform.position = Transforms.pos_caseUprightOnDesk;
                spawnedCase.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                // spawn other parts that attach to case when upright
                foreach (InventoryPart attachment in scenarioItems)
                {
                    if (!attachment.isStowed)
                    {
                        if (attachment.partType == InventoryPartType.SSDSata)
                            SpawnPart(attachment.partId, Transforms.pos_ssdSataOnCase, Transforms.rot_sataOnCase, spawnedCase);
                        else if (attachment.partType == InventoryPartType.HDD)
                            SpawnPart(attachment.partId, Transforms.pos_hddOnCase, Transforms.rot_sataOnCase, spawnedCase);
                        else if (attachment.partType == InventoryPartType.PSU)
                            SpawnPart(attachment.partId, Transforms.pos_psuOnCase, Transforms.rot_psuOnCase, spawnedCase);
                        else if (attachment.partType == InventoryPartType.SystemFan)
                        {
                            Port targetPort = caseToSpawn.attachList.Find(port => port.occupiedByPartId == attachment.partId);

                            if (targetPort.portName == "System Fan (Front1)")
                                SpawnPart(attachment.partId, Transforms.pos_systemFanOnCaseFront1, Transforms.rot_systemFanOnCaseFront, spawnedCase);
                            else if (targetPort.portName == "System Fan (Front2)")
                                SpawnPart(attachment.partId, Transforms.pos_systemFanOnCaseFront2, Transforms.rot_systemFanOnCaseFront, spawnedCase);
                            else if (targetPort.portName == "System Fan (Front3)")
                                SpawnPart(attachment.partId, Transforms.pos_systemFanOnCaseFront3, Transforms.rot_systemFanOnCaseFront, spawnedCase);
                            else if (targetPort.portName == "System Fan (Top1)")
                                SpawnPart(attachment.partId, Transforms.pos_systemFanOnCaseTop1, Transforms.rot_systemFanOnCaseTop, spawnedCase);
                            else if (targetPort.portName == "System Fan (Top2)")
                                SpawnPart(attachment.partId, Transforms.pos_systemFanOnCaseTop2, Transforms.rot_systemFanOnCaseTop, spawnedCase);
                            else if (targetPort.portName == "System Fan (Rear)")
                                SpawnPart(attachment.partId, Transforms.pos_systemFanOnCaseRear, Transforms.rot_systemFanOnCaseRear, spawnedCase);
                        }
                    }
                }

            } else
            {
                // cancel spawning as there is probably no non-stowed case in the scenario
                Destroy(spawnedMobo);
                return;
            }

        } else
        {
            // cancel spawning as the motherboard is probably stowed
            return;
        }

    }

    public void SwitchCameraView(GameEnums.CameraView cameraView)
    {
        List<GameObject> inactiveCameraAngles = new();
        GameObject cameraAngles = GameObject.Find("CameraAngles");
        string nextCameraView = cameraView switch {
            GameEnums.CameraView.DefaultTableView => "DefaultTableView",
            GameEnums.CameraView.OverheadView => "OverheadView",
            GameEnums.CameraView.CpuAttachmentView => "CpuAttachmentView",
            GameEnums.CameraView.RamAttachmentView => "RamAttachmentView",
            GameEnums.CameraView.CoolerAttachmentView => "CoolerAttachmentView",
            GameEnums.CameraView.m2AttachmentView => "m2AttachmentView",
            GameEnums.CameraView.IoShieldAttachmentView => "IoShieldAttachmentView",
            GameEnums.CameraView.MoboAttachmentView => "MoboAttachmentView",
            GameEnums.CameraView.CaseFanFrontAttachmentView => "CaseFanFrontAttachmentView",
            GameEnums.CameraView.CaseFanTopAttachmentView => "CaseFanTopAttachmentView",
            GameEnums.CameraView.PsuAttachmentView => "PsuAttachmentView",
            GameEnums.CameraView.SataStorageAttachmentView => "SataStorageAttachmentView",
            GameEnums.CameraView.MonitorView => "MonitorView",
            GameEnums.CameraView.DefaultTableView_rear => "DefaultTableView_rear",
            GameEnums.CameraView.DefaultTableView_left => "DefaultTableView_left",
            GameEnums.CameraView.DefaultTableView_right => "DefaultTableView_right",
            _ => null
        };

        GameObject cameraAngle;
        for (int i = 0; i < cameraAngles.transform.childCount; i++)
        {
            cameraAngle = cameraAngles.transform.GetChild(i).gameObject;
            if (cameraAngle.name == nextCameraView)
            {
                cameraAngle.SetActive(true);
            } else
            {
                inactiveCameraAngles.Add(cameraAngle);
            }
        }

        foreach (GameObject inactiveCameraAngle in inactiveCameraAngles)
        {
            inactiveCameraAngle.SetActive(false);
        }
    }

    public void SwitchCameraViewFront ()
    {
        SwitchCameraView(GameEnums.CameraView.DefaultTableView);
    }
    public void SwitchCameraViewRear()
    {
        SwitchCameraView(GameEnums.CameraView.DefaultTableView_rear);
    }
    public void SwitchCameraViewLeft ()
    {
        SwitchCameraView(GameEnums.CameraView.DefaultTableView_left);
    }
    public void SwitchCameraViewRight ()
    {
        SwitchCameraView(GameEnums.CameraView.DefaultTableView_right);
    }
    public void SwitchCameraViewOverhead ()
    {
        SwitchCameraView(GameEnums.CameraView.OverheadView);
    }


    public string GetCurrentCameraView()
    {
        GameObject cameraAngles = GameObject.Find("CameraAngles");
        GameObject cameraAngle;
        for (int i = 0; i < cameraAngles.transform.childCount; i++)
        {
            cameraAngle = cameraAngles.transform.GetChild(i).gameObject;
            if (cameraAngle.activeInHierarchy)
            {
                return cameraAngle.name;
            }
        }
        return null;
    }

    public void SpawnPart(string spawnId, Vector3 position, Vector3 rotation, GameObject parent)
    {
        InventoryPart partToSpawn = scenarioItems.Find(item => item.partId == spawnId);
        if (partToSpawn != null)
        {
            var spawnedPrefab = Instantiate(Resources.Load<GameObject>($"Prefabs/{partToSpawn.prefabId}"));
            spawnedPrefab.name = partToSpawn.partId;
            spawnedPrefab.transform.position = position;
            spawnedPrefab.transform.localEulerAngles = rotation;
            spawnedPrefab.AddComponent<PrefabPartIdController>();
            spawnedPrefab.GetComponent<PrefabPartIdController>().partId = partToSpawn.partId;
            if (parent != null)
                spawnedPrefab.transform.parent = parent.transform;
            else
                spawnedPrefab.transform.parent = null;
        }
        else
        {
            Debug.Log("Can't spawn part that is null!");
        }
    }

    public void SelectPartFromInventoryDisplay(string selectedPartId)
    {
        // IconController will call this method when IconContainer is clicked
        InventoryPart selectedPart = CheckCurrentInventory(selectedPartId);

        if (selectedPart != null)
        {
            if (selectedPart.isStowed)
            {
                AttachSelectedPart(selectedPart);
            }
        }
    }

    public void AttachSelectedPart(InventoryPart selectedPart)
    {
        // TODO handle attaching part to another part
        //     triggered by attach action on selected part
        //     check part type
        //     invoke appropriate attachment mode (view, steps, etc) according to part type
        if (selectedPart.partType == InventoryPartType.Motherboard)
            AttachMotherboard(selectedPart);
        else if (selectedPart.partType == InventoryPartType.SSDSata)
            AttachSataSsd(selectedPart);
        else if (selectedPart.partType == InventoryPartType.HDD)
            AttachHdd(selectedPart);
        else if (selectedPart.partType == InventoryPartType.PSU)
            AttachPsu(selectedPart);
        else if (selectedPart.partType == InventoryPartType.GPU)
        {
            // call popup canvas here
            ShowGpuAttachmentPopup(selectedPart);
        }
        else if (selectedPart.partType == InventoryPartType.SystemFan)
            // call popup canvas here
            ShowSystemFanAttachmentPopup(selectedPart);
        else if (selectedPart.partType == InventoryPartType.CPU)
            AttachCpu(selectedPart);
        else if (selectedPart.partType == InventoryPartType.CPUCooler)
            AttachCpuCooler(selectedPart);
        else if (selectedPart.partType == InventoryPartType.RAM)
        {
            // call a popup canvas with choices here
            ShowRamAttachmentPopup(selectedPart);
        }
        else if (selectedPart.partType == InventoryPartType.SSDM2)
            AttachM2Ssd(selectedPart);
        else if (selectedPart.partType == InventoryPartType.Case)
            AttachCase(selectedPart);
    }

    public void AttachCase(InventoryPart selectedPart)
    {
        print("attaching case!");
        // attach case
        // check first if there is a mobo in the table
        InventoryPart moboToSetAside = scenarioItems.Find(item => !item.isStowed && item.partType == InventoryPartType.Motherboard);
        if (moboToSetAside != null)
        {
            // if there are items in the table, e.g. motherboard assembly, temporarily set them aside first
            GameObject.Find($"{moboToSetAside.partId}").transform.position = Transforms.pos_moboOnDeskSetAside;

            // TODO enable button for placing motherboard on case

        }
        // if none proceed normally

        // spawn case laid
        SpawnPart(selectedPart.partId, Transforms.pos_caseLaidOnDesk, Transforms.rot_caseLaidOnDesk, null);

        // install IO shield
        var obj = Instantiate(Resources.Load<GameObject>("Prefabs/ioShield"));
        obj.transform.position = Transforms.pos_ioShieldOnCase;
        obj.transform.localEulerAngles = Transforms.rot_ioShieldOnCase;
        obj.transform.parent = GameObject.Find($"{selectedPart.partId}").transform;

        SwitchCameraView(GameEnums.CameraView.DefaultTableView);

        // unstow case
        UnstowItem(selectedPart.partId);
    }

    public void AttachMotherboardSetAside ()
    {
        // attach motherboard that is set aside
        InventoryPart moboToAttach = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard
                                                        && !item.isStowed);
        if (moboToAttach != null)
        {
            // check for existence of case
            InventoryPartCase
                caseToAttach = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed)
                as InventoryPartCase;

            if (caseToAttach != null)
            {
                // begin attaching
                GameObject caseObject = GameObject.Find(caseToAttach.partId);
                GameObject moboObject = GameObject.Find(moboToAttach.partId);

                if (caseObject != null && moboObject != null)
                {
                    Port targetPort = caseToAttach.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo
                                                                   && string.IsNullOrEmpty(item.occupiedByPartId));

                    if (targetPort != null)
                    {

                        SwitchCameraView(GameEnums.CameraView.MoboAttachmentView);

                        caseObject.transform.position = Transforms.pos_caseLaidOnDesk;
                        caseObject.transform.localEulerAngles = Transforms.rot_caseLaidOnDesk;
                        moboObject.transform.position = Transforms.pos_moboMountedOnCase;
                        moboObject.transform.parent = caseObject.transform;
                        targetPort.occupiedByPartId = moboToAttach.partId;
                    }
                }
            }


        } else
        {
            // can't attach what is not unstowed
        }
    }

    public void ToggleLeftSidePanel ()
    {
        InventoryPart checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed);
        GameObject sidePanel;
        if (checkedCase != null)
        {
            sidePanel = GameObject.Find($"{checkedCase.partId}").transform.GetChild(6).gameObject;
            if (sidePanel.activeInHierarchy == false)
            {
                sidePanel.SetActive(true);
            } else
            {
                sidePanel.SetActive(false);
            }
        }
    }

    public void ToggleRightSidePanel()
    {
        InventoryPart checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed);
        GameObject sidePanel;
        if (checkedCase != null)
        {
            sidePanel = GameObject.Find($"{checkedCase.partId}").transform.GetChild(0).gameObject;
            if (sidePanel.activeInHierarchy == false)
            {
                sidePanel.SetActive(true);
            }
            else
            {
                sidePanel.SetActive(false);
            }
        }
    }

    public void AttachMotherboard(InventoryPart selectedPart)
    {
        // attach motherboard, triggered by AttachSelectedPart
        // check first if case already has motherboard
        InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
        if (checkedCase != null)
        {
            Debug.Log("case present");
            if (checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_mobo && string.IsNullOrEmpty(port.occupiedByPartId)) == null)
            {
                // can't attach motherboard if case is already occupied
            }
            else
            {
                // set aside case
                GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseWorkingOnDesk;
                GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                // SwitchCameraView(GameEnums.CameraView.MoboAttachmentView);

                //     spawn, set parent, and set parent's port as occupied
                SpawnPart(selectedPart.partId, Transforms.pos_moboOnDesk, Transforms.rot_moboOnDesk, null);
                //Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_mobo);
                //if (targetPort != null)
                //{
                //    targetPort.occupiedByPartId = selectedPart.partId;
                //}

                //     unstow item
                UnstowItem(selectedPart.partId);
            }
        }
        else
        {
            Debug.Log("case absent");
            // place motherboard on desk if there is no case on desk

            // check if there is non-stowed motherboard
            InventoryPart checkedMotherboard = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard
                                                                  && !item.isStowed);

            if (checkedMotherboard != null)
            {
                // can't spawn another motherboard as there is already one on desk
            } else
            {
                // spawn motherboard
                SpawnPart(selectedPart.partId, Transforms.pos_moboOnDesk, Transforms.rot_moboOnDesk, null);

                UnstowItem(selectedPart.partId);
            }
        }
    }

    public void AttachSataSsd(InventoryPart selectedPart)
    {
        // attach ssd, triggered by AttachSelectedPart
        // check first if case already has sata ssd
        InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
        if (checkedCase != null)
        {
            Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_ssdBay && string.IsNullOrEmpty(port.occupiedByPartId));
            if (targetPort != null)
            {
                InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                if (checkedMobo != null)
                {
                    if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                    {
                        // set aside motherboard
                        GameObject moboObject = GameObject.Find(checkedMobo.partId);
                        moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                    }
                }

                // attach ssd here
                //     set case transform to upright
                GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                SwitchCameraView(GameEnums.CameraView.SataStorageAttachmentView);

                targetPort.occupiedByPartId = selectedPart.partId;

                //     spawn and set parent
                SpawnPart(selectedPart.partId, Transforms.pos_ssdSataOnCase, Transforms.rot_sataOnCase, GameObject.Find(checkedCase.partId));

                //     unstow item
                UnstowItem(selectedPart.partId);
            }
            else
            {
                // can't attach ssd if case is already occupied
            }
        }
        else
        {
            // can't attach ssd if there is no motherboard
        }
    }

    public void AttachHdd(InventoryPart selectedPart)
    {
        print("attaching hdd!");
        // attach hdd, triggered by AttachSelectedPart
        // check first if case already has hdd
        InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
        if (checkedCase != null)
        {
            print("case present");
            Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_hddBay && string.IsNullOrEmpty(port.occupiedByPartId));
            if (targetPort != null)
            {
                InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                if (checkedMobo != null)
                {
                    if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                    {
                        // set aside motherboard
                        GameObject moboObject = GameObject.Find(checkedMobo.partId);
                        moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                    }
                }

                // attach hdd here
                //     set case transform to upright
                GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                SwitchCameraView(GameEnums.CameraView.SataStorageAttachmentView);

                targetPort.occupiedByPartId = selectedPart.partId;

                //     spawn and set parent
                SpawnPart(selectedPart.partId, Transforms.pos_hddOnCase, Transforms.rot_sataOnCase, GameObject.Find(checkedCase.partId));

                //     unstow item
                UnstowItem(selectedPart.partId);
            }
            else
            {
                // can't attach hdd if case is already occupied
                print("cannot attach hdd, case occupied");
            }
        }
        else
        {
            // can't attach hdd if there is no case
        }
    }
    public void AttachPsu(InventoryPart selectedPart)
    {
        print("attaching psu!");
        // attach psu, triggered by AttachSelectedPart
        // check first if case already has psu
        InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
        if (checkedCase != null)
        {
            print("case present");
            Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_psuBasement && string.IsNullOrEmpty(port.occupiedByPartId));
            if (targetPort != null)
            {
                InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                if (checkedMobo != null)
                {
                    if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                    {
                        // set aside motherboard
                        GameObject moboObject = GameObject.Find(checkedMobo.partId);
                        moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                    }
                }

                // attach psu here
                //     set case transform to upright
                GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                SwitchCameraView(GameEnums.CameraView.PsuAttachmentView);

                targetPort.occupiedByPartId = selectedPart.partId;

                //     spawn and set parent
                SpawnPart(selectedPart.partId, Transforms.pos_psuOnCase, Transforms.rot_psuOnCase, GameObject.Find(checkedCase.partId));

                //     unstow item
                UnstowItem(selectedPart.partId);
            }
            else
            {
                print("cannot attach psu, case occupied");
                // can't attach psu if case is already occupied
            }
        }
        else
        {
            // can't attach hdd if there is no case
        }
    }

    public void AttachGpu(InventoryPart selectedPart, string position)
    {
        print("attaching gpu!");
        // attach gpu, triggered by AttachSelectedPart
        // check first if mobo already has gpu
        InventoryPartMotherboard checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed) as InventoryPartMotherboard;
        if (checkedMobo != null)
        {
            print("mobo present");
            // check if mobo is attached to case
            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
            if (checkedCase != null)
            {
                Port targetPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo && item.occupiedByPartId == checkedMobo.partId);
                if (targetPort != null)
                {
                    if (position == "top")
                    {
                        Port targetGpuPort = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_pcieX16
                                                                    && item.portName == "PCIe Slot (Top)"
                                                                    && string.IsNullOrEmpty(item.occupiedByPartId));
                        if (targetGpuPort != null)
                        {
                            targetGpuPort.occupiedByPartId = selectedPart.partId;

                            // lay down case
                            GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseLaidOnDesk;
                            GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseLaidOnDesk;

                            // switch view
                            SwitchCameraView(GameEnums.CameraView.MoboAttachmentView);

                            // spawn and set parent
                            SpawnPart(selectedPart.partId, Transforms.pos_gpuMountedOnMoboTop, Transforms.rot_gpuOnMobo, GameObject.Find(checkedCase.partId));

                            // unstow item
                            UnstowItem(selectedPart.partId);
                        } else
                        {
                            // can't attach if occupied
                        }
                    } else if (position == "bottom")
                    {
                        Port targetGpuPort = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_pcieX16
                                                                    && item.portName == "PCIe Slot (Bottom)"
                                                                    && string.IsNullOrEmpty(item.occupiedByPartId));
                        if (targetGpuPort != null)
                        {
                            targetGpuPort.occupiedByPartId = selectedPart.partId;

                            // lay down case
                            GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseLaidOnDesk;
                            GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseLaidOnDesk;

                            // switch view
                            SwitchCameraView(GameEnums.CameraView.MoboAttachmentView);

                            // spawn and set parent
                            SpawnPart(selectedPart.partId, Transforms.pos_gpuMountedOnMoboBottom, Transforms.rot_gpuOnMobo, GameObject.Find(checkedCase.partId));

                            // unstow item
                            UnstowItem(selectedPart.partId);
                        }
                        else
                        {
                            // can't attach if occupied
                        }
                    }
                } else
                {
                    // can't attach gpu if mobo is not attached to case
                }
            } else
            {
                // can't attach gpu if there is no case
            }
        }
        else
        {
            // can't attach case if there is no mobo
        }
    }

    public void AttachSystemFan(InventoryPart selectedPart, string position)
    {
        print("attaching system fan!");
        // attach psu, triggered by AttachSelectedPart
        // check first if case already has psu
        InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
        if (checkedCase != null)
        {
            print("case present");
            if (position == "front1")
            {
                Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_sysfan
                                                              && port.portName == "System Fan (Front1)"
                                                              && string.IsNullOrEmpty(port.occupiedByPartId));
                if (targetPort != null)
                {
                    // attach here
                    InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                    if (checkedMobo != null)
                    {
                        if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                        {
                            // set aside motherboard
                            GameObject moboObject = GameObject.Find(checkedMobo.partId);
                            moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                        }
                    }

                    // attach fan here
                    //     set case transform to upright
                    GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                    GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                    SwitchCameraView(GameEnums.CameraView.CaseFanFrontAttachmentView);

                    targetPort.occupiedByPartId = selectedPart.partId;

                    //     spawn and set parent
                    SpawnPart(selectedPart.partId, Transforms.pos_systemFanOnCaseFront1, Transforms.rot_systemFanOnCaseFront, GameObject.Find(checkedCase.partId));

                    //     unstow item
                    UnstowItem(selectedPart.partId);
                }
                else
                {
                    // fail to attach because attachment point is occupied
                }
            }
            else if (position == "front2")
            {
                Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_sysfan
                                                              && port.portName == "System Fan (Front2)"
                                                              && string.IsNullOrEmpty(port.occupiedByPartId));
                if (targetPort != null)
                {
                    // attach here
                    InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                    if (checkedMobo != null)
                    {
                        if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                        {
                            // set aside motherboard
                            GameObject moboObject = GameObject.Find(checkedMobo.partId);
                            moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                        }
                    }

                    // attach fan here
                    //     set case transform to upright
                    GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                    GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                    SwitchCameraView(GameEnums.CameraView.CaseFanFrontAttachmentView);

                    targetPort.occupiedByPartId = selectedPart.partId;

                    //     spawn and set parent
                    SpawnPart(selectedPart.partId, Transforms.pos_systemFanOnCaseFront2, Transforms.rot_systemFanOnCaseFront, GameObject.Find(checkedCase.partId));

                    //     unstow item
                    UnstowItem(selectedPart.partId);
                }
                else
                {
                    // fail to attach because attachment point is occupied
                }
            }
            else if (position == "front3")
            {
                Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_sysfan
                                                              && port.portName == "System Fan (Front3)"
                                                              && string.IsNullOrEmpty(port.occupiedByPartId));
                if (targetPort != null)
                {
                    // attach here
                    InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                    if (checkedMobo != null)
                    {
                        if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                        {
                            // set aside motherboard
                            GameObject moboObject = GameObject.Find(checkedMobo.partId);
                            moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                        }
                    }

                    // attach fan here
                    //     set case transform to upright
                    GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                    GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                    SwitchCameraView(GameEnums.CameraView.CaseFanFrontAttachmentView);

                    targetPort.occupiedByPartId = selectedPart.partId;

                    //     spawn and set parent
                    SpawnPart(selectedPart.partId, Transforms.pos_systemFanOnCaseFront3, Transforms.rot_systemFanOnCaseFront, GameObject.Find(checkedCase.partId));

                    //     unstow item
                    UnstowItem(selectedPart.partId);
                }
                else
                {
                    // fail to attach because attachment point is occupied
                }
            }
            else if (position == "top1")
            {
                Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_sysfan
                                                              && port.portName == "System Fan (Top1)"
                                                              && string.IsNullOrEmpty(port.occupiedByPartId));
                if (targetPort != null)
                {
                    // attach here
                    InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                    if (checkedMobo != null)
                    {
                        if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                        {
                            // set aside motherboard
                            GameObject moboObject = GameObject.Find(checkedMobo.partId);
                            moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                        }
                    }

                    // attach fan here
                    //     set case transform to upright
                    GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                    GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                    SwitchCameraView(GameEnums.CameraView.CaseFanTopAttachmentView);

                    targetPort.occupiedByPartId = selectedPart.partId;

                    //     spawn and set parent
                    SpawnPart(selectedPart.partId, Transforms.pos_systemFanOnCaseTop1, Transforms.rot_systemFanOnCaseTop, GameObject.Find(checkedCase.partId));

                    //     unstow item
                    UnstowItem(selectedPart.partId);
                }
                else
                {
                    // fail to attach because attachment point is occupied
                }
            }
            else if (position == "top2")
            {
                Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_sysfan
                                                              && port.portName == "System Fan (Top2)"
                                                              && string.IsNullOrEmpty(port.occupiedByPartId));
                if (targetPort != null)
                {
                    // attach here
                    InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                    if (checkedMobo != null)
                    {
                        if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                        {
                            // set aside motherboard
                            GameObject moboObject = GameObject.Find(checkedMobo.partId);
                            moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                        }
                    }

                    // attach fan here
                    //     set case transform to upright
                    GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                    GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                    SwitchCameraView(GameEnums.CameraView.CaseFanTopAttachmentView);

                    targetPort.occupiedByPartId = selectedPart.partId;

                    //     spawn and set parent
                    SpawnPart(selectedPart.partId, Transforms.pos_systemFanOnCaseTop2, Transforms.rot_systemFanOnCaseTop, GameObject.Find(checkedCase.partId));

                    //     unstow item
                    UnstowItem(selectedPart.partId);
                }
                else
                {
                    // fail to attach because attachment point is occupied
                }
            }
            else if (position == "rear")
            {
                Port targetPort = checkedCase.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_sysfan
                                                              && port.portName == "System Fan (Rear)"
                                                              && string.IsNullOrEmpty(port.occupiedByPartId));
                if (targetPort != null)
                {
                    // attach here
                    InventoryPart checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed);
                    if (checkedMobo != null)
                    {
                        if (checkedCase.attachList.Find(port => port.occupiedByPartId == checkedMobo.partId) == null)
                        {
                            // set aside motherboard
                            GameObject moboObject = GameObject.Find(checkedMobo.partId);
                            moboObject.transform.position = Transforms.pos_moboOnDeskSetAside;
                        }
                    }

                    // attach fan here
                    //     set case transform to upright
                    GameObject.Find(checkedCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;
                    GameObject.Find(checkedCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                    SwitchCameraView(GameEnums.CameraView.CaseFanTopAttachmentView);

                    targetPort.occupiedByPartId = selectedPart.partId;

                    //     spawn and set parent
                    SpawnPart(selectedPart.partId, Transforms.pos_systemFanOnCaseRear, Transforms.rot_systemFanOnCaseRear, GameObject.Find(checkedCase.partId));

                    //     unstow item
                    UnstowItem(selectedPart.partId);
                }
                else
                {
                    // fail to attach because attachment point is occupied
                }
            }
        }
        else
        {
            // can't attach fan if there is no case
        }
    }

    public void AttachCpu(InventoryPart selectedPart)
    {
        // check for nonstowed mobo
        InventoryPartMotherboard
            checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
            as InventoryPartMotherboard;

        if (checkedMobo != null)
        {
            // check if mobo is attached to case
            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
            if (checkedCase != null)
            {
                Port targetPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo && item.occupiedByPartId == checkedMobo.partId);
                if (targetPort == null)
                {
                    Port socket = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_cpu && string.IsNullOrEmpty(item.occupiedByPartId));

                    if (socket != null)
                    {
                        // if motherboard is set aside, adjust transform
                        // adjust transform of case first
                        GameObject spawnedCase = GameObject.Find(checkedCase.partId);
                        spawnedCase.transform.position = Transforms.pos_caseWorkingOnDesk;
                        spawnedCase.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                        GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                        spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                        spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                        socket.occupiedByPartId = selectedPart.partId;

                        // switch camera view
                        SwitchCameraView(GameEnums.CameraView.CpuAttachmentView);

                        // spawn and set parent
                        SpawnPart(selectedPart.partId, Transforms.pos_cpuMountedOnMobo, Transforms.rot_cpuMountedOnMobo, spawnedMobo);

                        // unstow item
                        UnstowItem(selectedPart.partId);

                    } else
                    {
                        // can't attach cpu if mobo socket is occupied
                    }
                }
                else
                {
                    // can't attach cpu if mobo is attached to case, remove motherboard from case first
                }
            }
            else
            {
                // attach here
                Port socket = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_cpu && string.IsNullOrEmpty(item.occupiedByPartId));

                if (socket != null)
                {
                    // adjust transform
                    GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                    spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                    spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                    socket.occupiedByPartId = selectedPart.partId;

                    // switch camera view
                    SwitchCameraView(GameEnums.CameraView.CpuAttachmentView);

                    // spawn and set parent
                    SpawnPart(selectedPart.partId, Transforms.pos_cpuMountedOnMobo, Transforms.rot_cpuMountedOnMobo, spawnedMobo);

                    // unstow item
                    UnstowItem(selectedPart.partId);
                }
                else
                {
                    // can't attach cpu if mobo socket is occupied
                }
            }
        } else
        {
            // can't spawn cpu as there is no non-stowed motherboard
        }

    }
    public void AttachCpuCooler(InventoryPart selectedPart)
    {
        // check for nonstowed mobo
        InventoryPartMotherboard
            checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
            as InventoryPartMotherboard;

        if (checkedMobo != null)
        {
            // check if mobo is attached to case
            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
            if (checkedCase != null)
            {
                Port targetPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo && item.occupiedByPartId == checkedMobo.partId);
                if (targetPort == null)
                {
                    // check if cpu socket is occupied
                    Port socket = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_cpu && !string.IsNullOrEmpty(item.occupiedByPartId));

                    if (socket != null)
                    { 
                        // check if cooler bracket is occupied
                        Port coolerBracket = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_cpuCoolerBracket && string.IsNullOrEmpty(item.occupiedByPartId));
                        if (coolerBracket != null)
                        {
                            // if motherboard is set aside, adjust transform
                            // adjust transform of case first
                            GameObject spawnedCase = GameObject.Find(checkedCase.partId);
                            spawnedCase.transform.position = Transforms.pos_caseWorkingOnDesk;
                            spawnedCase.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                            GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                            spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                            spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                            coolerBracket.occupiedByPartId = selectedPart.partId;

                            // switch camera view
                            SwitchCameraView(GameEnums.CameraView.CpuAttachmentView);

                            // spawn and set parent
                            SpawnPart(selectedPart.partId, Transforms.pos_cpuCoolerMountedOnMobo, Transforms.rot_cpuCoolerOnMobo, spawnedMobo);

                            // unstow item
                            UnstowItem(selectedPart.partId);
                        } else
                        {
                            // can't attach cooler if bracket is already occupied
                        }
                    }
                    else
                    {
                        // can't attach cooler if there is no cpu in mobo socket
                    }
                }
                else
                {
                    // can't attach cpu if mobo is attached to case, remove motherboard from case first
                }
            }
            else
            {
                // attach here
                // check if cpu socket is occupied
                Port socket = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_cpu && !string.IsNullOrEmpty(item.occupiedByPartId));

                if (socket != null)
                {
                    // check if cooler bracket is occupied
                    Port coolerBracket = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_cpuCoolerBracket && string.IsNullOrEmpty(item.occupiedByPartId));
                    if (coolerBracket != null)
                    {
                        // adjust transform
                        
                        GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                        spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                        spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                        coolerBracket.occupiedByPartId = selectedPart.partId;

                        // switch camera view
                        SwitchCameraView(GameEnums.CameraView.CpuAttachmentView);

                        // spawn and set parent
                        SpawnPart(selectedPart.partId, Transforms.pos_cpuCoolerMountedOnMobo, Transforms.rot_cpuCoolerOnMobo, spawnedMobo);

                        // unstow item
                        UnstowItem(selectedPart.partId);
                    }
                    else
                    {
                        // can't attach cooler if bracket is already occupied
                    }
                }
                else
                {
                    // can't attach cooler if there is no cpu in mobo socket
                }
            }
        }
        else
        {
            // can't spawn cooler as there is no non-stowed motherboard
        }
    }
    public void AttachRam(InventoryPart selectedPart, string position)
    {
        // check for nonstowed mobo
        InventoryPartMotherboard
            checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
            as InventoryPartMotherboard;

        if (checkedMobo != null)
        {
            // check if mobo is attached to case
            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
            if (checkedCase != null)
            {
                Port targetPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo && item.occupiedByPartId == checkedMobo.partId);
                if (targetPort == null)
                {
                    if (position == "dimm1")
                    {
                        Port targetSlot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_dram
                                                                    && item.portName == "DIMM Slot 1"
                                                                    && string.IsNullOrEmpty(item.occupiedByPartId));
                        if (targetSlot != null)
                        {
                            targetSlot.occupiedByPartId = selectedPart.partId;

                            // adjust transforms
                            GameObject spawnedCase = GameObject.Find(checkedCase.partId);
                            spawnedCase.transform.position = Transforms.pos_caseWorkingOnDesk;
                            spawnedCase.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                            GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                            spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                            spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                            // switch view
                            SwitchCameraView(GameEnums.CameraView.RamAttachmentView);

                            // spawn and set parent
                            SpawnPart(selectedPart.partId, Transforms.pos_dimm1MountedOnMobo, Transforms.rot_dimmOnMobo, GameObject.Find(checkedMobo.partId));

                            // unstow item
                            UnstowItem(selectedPart.partId);
                        }
                        else
                        {
                            // can't attach if occupied
                        }
                    }
                    else if (position == "dimm2")
                    {
                        Port targetSlot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_dram
                                                                    && item.portName == "DIMM Slot 2"
                                                                    && string.IsNullOrEmpty(item.occupiedByPartId));
                        if (targetSlot != null)
                        {
                            targetSlot.occupiedByPartId = selectedPart.partId;

                            // adjust transforms
                            GameObject spawnedCase = GameObject.Find(checkedCase.partId);
                            spawnedCase.transform.position = Transforms.pos_caseWorkingOnDesk;
                            spawnedCase.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                            GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                            spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                            spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                            // switch view
                            SwitchCameraView(GameEnums.CameraView.RamAttachmentView);

                            // spawn and set parent
                            SpawnPart(selectedPart.partId, Transforms.pos_dimm2MountedOnMobo, Transforms.rot_dimmOnMobo, GameObject.Find(checkedMobo.partId));

                            // unstow item
                            UnstowItem(selectedPart.partId);
                        }
                        else
                        {
                            // can't attach if occupied
                        }
                    }
                    if (position == "dimm3")
                    {
                        Port targetSlot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_dram
                                                                    && item.portName == "DIMM Slot 3"
                                                                    && string.IsNullOrEmpty(item.occupiedByPartId));
                        if (targetSlot != null)
                        {
                            targetSlot.occupiedByPartId = selectedPart.partId;

                            // adjust transforms
                            GameObject spawnedCase = GameObject.Find(checkedCase.partId);
                            spawnedCase.transform.position = Transforms.pos_caseWorkingOnDesk;
                            spawnedCase.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                            GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                            spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                            spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                            // switch view
                            SwitchCameraView(GameEnums.CameraView.RamAttachmentView);

                            // spawn and set parent
                            SpawnPart(selectedPart.partId, Transforms.pos_dimm3MountedOnMobo, Transforms.rot_dimmOnMobo, GameObject.Find(checkedMobo.partId));

                            // unstow item
                            UnstowItem(selectedPart.partId);
                        }
                        else
                        {
                            // can't attach if occupied
                        }
                    }
                    if (position == "dimm4")
                    {
                        Port targetSlot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_dram
                                                                    && item.portName == "DIMM Slot 4"
                                                                    && string.IsNullOrEmpty(item.occupiedByPartId));
                        if (targetSlot != null)
                        {
                            targetSlot.occupiedByPartId = selectedPart.partId;

                            // adjust transforms
                            GameObject spawnedCase = GameObject.Find(checkedCase.partId);
                            spawnedCase.transform.position = Transforms.pos_caseWorkingOnDesk;
                            spawnedCase.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                            GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                            spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                            spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                            // switch view
                            SwitchCameraView(GameEnums.CameraView.RamAttachmentView);

                            // spawn and set parent
                            SpawnPart(selectedPart.partId, Transforms.pos_dimm4MountedOnMobo, Transforms.rot_dimmOnMobo, GameObject.Find(checkedMobo.partId));

                            // unstow item
                            UnstowItem(selectedPart.partId);
                        }
                        else
                        {
                            // can't attach if occupied
                        }
                    }
                }
                else
                {
                    // can't attach ram if mobo is attached to case, remove motherboard from case first
                }
            }
            else
            {
                // attach here
                if (position == "dimm1")
                {
                    Port targetSlot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_dram
                                                                && item.portName == "DIMM Slot 1"
                                                                && string.IsNullOrEmpty(item.occupiedByPartId));
                    if (targetSlot != null)
                    {
                        targetSlot.occupiedByPartId = selectedPart.partId;

                        // adjust transforms
                        GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                        spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                        spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                        // switch view
                        SwitchCameraView(GameEnums.CameraView.RamAttachmentView);

                        // spawn and set parent
                        SpawnPart(selectedPart.partId, Transforms.pos_dimm1MountedOnMobo, Transforms.rot_dimmOnMobo, GameObject.Find(checkedMobo.partId));

                        // unstow item
                        UnstowItem(selectedPart.partId);
                    }
                    else
                    {
                        // can't attach if occupied
                    }
                }
                else if (position == "dimm2")
                {
                    Port targetSlot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_dram
                                                                && item.portName == "DIMM Slot 2"
                                                                && string.IsNullOrEmpty(item.occupiedByPartId));
                    if (targetSlot != null)
                    {
                        targetSlot.occupiedByPartId = selectedPart.partId;

                        // adjust transforms
                        GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                        spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                        spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                        // switch view
                        SwitchCameraView(GameEnums.CameraView.RamAttachmentView);

                        // spawn and set parent
                        SpawnPart(selectedPart.partId, Transforms.pos_dimm2MountedOnMobo, Transforms.rot_dimmOnMobo, GameObject.Find(checkedMobo.partId));

                        // unstow item
                        UnstowItem(selectedPart.partId);
                    }
                    else
                    {
                        // can't attach if occupied
                    }
                }
                if (position == "dimm3")
                {
                    Port targetSlot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_dram
                                                                && item.portName == "DIMM Slot 3"
                                                                && string.IsNullOrEmpty(item.occupiedByPartId));
                    if (targetSlot != null)
                    {
                        targetSlot.occupiedByPartId = selectedPart.partId;

                        // adjust transforms
                        GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                        spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                        spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                        // switch view
                        SwitchCameraView(GameEnums.CameraView.RamAttachmentView);

                        // spawn and set parent
                        SpawnPart(selectedPart.partId, Transforms.pos_dimm3MountedOnMobo, Transforms.rot_dimmOnMobo, GameObject.Find(checkedMobo.partId));

                        // unstow item
                        UnstowItem(selectedPart.partId);
                    }
                    else
                    {
                        // can't attach if occupied
                    }
                }
                if (position == "dimm4")
                {
                    Port targetSlot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_dram
                                                                && item.portName == "DIMM Slot 4"
                                                                && string.IsNullOrEmpty(item.occupiedByPartId));
                    if (targetSlot != null)
                    {
                        targetSlot.occupiedByPartId = selectedPart.partId;

                        // adjust transforms
                        GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                        spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                        spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                        // switch view
                        SwitchCameraView(GameEnums.CameraView.RamAttachmentView);

                        // spawn and set parent
                        SpawnPart(selectedPart.partId, Transforms.pos_dimm4MountedOnMobo, Transforms.rot_dimmOnMobo, GameObject.Find(checkedMobo.partId));

                        // unstow item
                        UnstowItem(selectedPart.partId);
                    }
                    else
                    {
                        // can't attach if occupied
                    }
                }
            }
        }
        else
        {
            // can't spawn ram as there is no non-stowed motherboard
        }
    }
    public void AttachM2Ssd(InventoryPart selectedPart)
    {
        // check for nonstowed mobo
        InventoryPartMotherboard
            checkedMobo = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
            as InventoryPartMotherboard;

        if (checkedMobo != null)
        {
            // check if mobo is attached to case
            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
            if (checkedCase != null)
            {
                Port targetPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo && item.occupiedByPartId == checkedMobo.partId);
                if (targetPort == null)
                {
                    Port m2Slot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_m2 && string.IsNullOrEmpty(item.occupiedByPartId));

                    if (m2Slot != null)
                    {
                        // if motherboard is set aside, adjust transform
                        // adjust transform of case first
                        GameObject spawnedCase = GameObject.Find(checkedCase.partId);
                        spawnedCase.transform.position = Transforms.pos_caseWorkingOnDesk;
                        spawnedCase.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;

                        GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                        spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                        spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                        m2Slot.occupiedByPartId = selectedPart.partId;

                        // switch camera view
                        SwitchCameraView(GameEnums.CameraView.m2AttachmentView);

                        // spawn and set parent
                        SpawnPart(selectedPart.partId, Transforms.pos_m2MountedOnMobo, Transforms.rot_m2OnMobo, spawnedMobo);

                        // unstow item
                        UnstowItem(selectedPart.partId);

                    }
                    else
                    {
                        // can't attach m2 drive if slot is occupied
                    }
                }
                else
                {
                    // can't attach m2 drive if mobo is attached to case, remove motherboard from case first
                }
            }
            else
            {
                // attach here
                Port m2Slot = checkedMobo.ports.Find(item => item.portType == GameEnums.PortType.mobo_m2 && string.IsNullOrEmpty(item.occupiedByPartId));

                if (m2Slot != null)
                {
                    // adjust transform
                    GameObject spawnedMobo = GameObject.Find(checkedMobo.partId);
                    spawnedMobo.transform.position = Transforms.pos_moboOnDesk;
                    spawnedMobo.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                    m2Slot.occupiedByPartId = selectedPart.partId;

                    // switch camera view
                    SwitchCameraView(GameEnums.CameraView.m2AttachmentView);

                    // spawn and set parent
                    SpawnPart(selectedPart.partId, Transforms.pos_m2MountedOnMobo, Transforms.rot_m2OnMobo, spawnedMobo);

                    // unstow item
                    UnstowItem(selectedPart.partId);

                }
                else
                {
                    // can't attach m2 drive if slot is occupied
                }
            }
        }
        else
        {
            // can't spawn cpu as there is no non-stowed motherboard
        }
    }


    public void ActivateRemovalMode()
    {
        selectionModeEnabled = true;
        var obj = Instantiate(Resources.Load<GameObject>("UIPrefabs/PartRemovalCanvas"), transform.parent);
    }

    public void DetachSelectedPart ()
    {
        // TODO handle detaching part from another part
        //     triggered by detach action on selected part
        //     check part type
        //     invoke appropriate detachment mode (view, steps, etc) according to part type

        if (!string.IsNullOrEmpty(selectedPartId))
        {
            Debug.Log($"Stubbed removal of {selectedPartId}");
            InventoryPart partToDetach = CheckCurrentInventory(selectedPartId);
            if (partToDetach != null)
            {
                if (!partToDetach.isStowed)
                {
                    if (partToDetach.partType == InventoryPartType.Case)
                    {
                        // check if anything is attached to the case
                        if (((InventoryPartCase)partToDetach).attachList.Find(port => !string.IsNullOrEmpty(port.occupiedByPartId)) == null)
                        {
                            GameObject objectToRemove = GameObject.Find(partToDetach.partId);
                            if (objectToRemove != null)
                            {
                                Destroy(objectToRemove);
                                partToDetach.isStowed = true;
                            }
                        } else
                        {
                            // can't detach case with attachments!
                        }
                    } else if (partToDetach.partType == InventoryPartType.CPU)
                    {
                        // cpu is most likely attached to motherboard
                        // check if motherboard is attached to case
                        InventoryPartMotherboard
                            moboToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
                            as InventoryPartMotherboard;
                        if (moboToCheck != null)
                        {
                            print("good, mobo is present");
                            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case
                                                                               && !item.isStowed) as InventoryPartCase;
                            if (checkedCase != null)
                            {
                                print("case is present");
                                Port checkedPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo
                                                                               && item.occupiedByPartId == moboToCheck.partId);
                                if (checkedPort == null)
                                {
                                    print("good, mobo is not attached to case");
                                    // good to go, remove
                                    GameObject objectToRemove = GameObject.Find(moboToCheck.partId).transform.Find(partToDetach.partId).gameObject;
                                    if (objectToRemove != null)
                                    {
                                        print("removing object");
                                        Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_cpu
                                                                                 && port.occupiedByPartId == partToDetach.partId);
                                        if (targetPort != null)
                                        {
                                            print("removing from cpu socket and destroying");
                                            targetPort.occupiedByPartId = null;
                                            Destroy(objectToRemove);
                                            partToDetach.isStowed = true;
                                        }
                                    }

                                } else
                                {
                                    // can't detach when mobo is attached to case!
                                    print("can't detach parts attached to mobo, remove mobo assembly from case first!");
                                }
                            } else
                            {
                                // no case in world, remove cpu
                                print("good, no case in world space");
                                // good to go, remove
                                GameObject objectToRemove = GameObject.Find(moboToCheck.partId).transform.Find(partToDetach.partId).gameObject;
                                if (objectToRemove != null)
                                {
                                    print("removing object");
                                    Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_cpu
                                                                             && port.occupiedByPartId == partToDetach.partId);
                                    if (targetPort != null)
                                    {
                                        print("removing from cpu socket and destroying");
                                        targetPort.occupiedByPartId = null;
                                        Destroy(objectToRemove);
                                        partToDetach.isStowed = true;
                                    }
                                }
                            }
                        }
                    } else if (partToDetach.partType == InventoryPartType.CPUCooler)
                    {
                        // check if motherboard is attached to case
                        InventoryPartMotherboard
                            moboToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
                            as InventoryPartMotherboard;
                        if (moboToCheck != null)
                        {
                            print("good, mobo is present");
                            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case
                                                                               && !item.isStowed) as InventoryPartCase;
                            if (checkedCase != null)
                            {
                                print("case is present");
                                Port checkedPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo
                                                                               && item.occupiedByPartId == moboToCheck.partId);
                                if (checkedPort == null)
                                {
                                    print("good, mobo is not attached to case");
                                    // good to go, remove
                                    GameObject objectToRemove = GameObject.Find(moboToCheck.partId).transform.Find(partToDetach.partId).gameObject;
                                    if (objectToRemove != null)
                                    {
                                        print("removing object");
                                        Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_cpuCoolerBracket
                                                                                 && port.occupiedByPartId == partToDetach.partId);
                                        if (targetPort != null)
                                        {
                                            print("removing from bracket and destroying");
                                            targetPort.occupiedByPartId = null;
                                            Destroy(objectToRemove);
                                            partToDetach.isStowed = true;
                                        }
                                    }
                                }
                                else
                                {
                                    // can't detach when mobo is attached to case!
                                    print("can't detach parts attached to mobo, remove mobo assembly from case first!");
                                }
                            }
                            else
                            {
                                // no case in world, remove cooler
                                print("good, no case in world space");
                                // good to go, remove
                                GameObject objectToRemove = GameObject.Find(moboToCheck.partId).transform.Find(partToDetach.partId).gameObject;
                                if (objectToRemove != null)
                                {
                                    print("removing object");
                                    Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_cpuCoolerBracket
                                                                             && port.occupiedByPartId == partToDetach.partId);
                                    if (targetPort != null)
                                    {
                                        print("removing from bracket and destroying");
                                        targetPort.occupiedByPartId = null;
                                        Destroy(objectToRemove);
                                        partToDetach.isStowed = true;
                                    }
                                }
                            }
                        }
                    } else if (partToDetach.partType == InventoryPartType.RAM)
                    {
                        // check if motherboard is attached to case
                        InventoryPartMotherboard
                            moboToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
                            as InventoryPartMotherboard;
                        if (moboToCheck != null)
                        {
                            print("good, mobo is present");
                            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case
                                                                               && !item.isStowed) as InventoryPartCase;
                            if (checkedCase != null)
                            {
                                print("case is present");
                                Port checkedPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo
                                                                               && item.occupiedByPartId == moboToCheck.partId);
                                if (checkedPort == null)
                                {
                                    print("good, mobo is not attached to case");
                                    // good to go, remove
                                    GameObject objectToRemove = GameObject.Find(moboToCheck.partId).transform.Find(partToDetach.partId).gameObject;
                                    if (objectToRemove != null)
                                    {
                                        print("removing object");
                                        Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_dram
                                                                                 && port.occupiedByPartId == partToDetach.partId);
                                        if (targetPort != null)
                                        {
                                            print("removing from bracket and destroying");
                                            targetPort.occupiedByPartId = null;
                                            Destroy(objectToRemove);
                                            partToDetach.isStowed = true;
                                        }
                                    }
                                }
                                else
                                {
                                    // can't detach when mobo is attached to case!
                                    print("can't detach parts attached to mobo, remove mobo assembly from case first!");
                                }
                            }
                            else
                            {
                                // no case in world, remove ram
                                print("good, no case in world space");
                                // good to go, remove
                                GameObject objectToRemove = GameObject.Find(moboToCheck.partId).transform.Find(partToDetach.partId).gameObject;
                                if (objectToRemove != null)
                                {
                                    print("removing object");
                                    Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_dram
                                                                             && port.occupiedByPartId == partToDetach.partId);
                                    if (targetPort != null)
                                    {
                                        print("removing from bracket and destroying");
                                        targetPort.occupiedByPartId = null;
                                        Destroy(objectToRemove);
                                        partToDetach.isStowed = true;
                                    }
                                }
                            }
                        }
                    } else if (partToDetach.partType == InventoryPartType.SSDM2)
                    {
                        // check if motherboard is attached to case
                        InventoryPartMotherboard
                            moboToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
                            as InventoryPartMotherboard;
                        if (moboToCheck != null)
                        {
                            print("good, mobo is present");
                            InventoryPartCase checkedCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case
                                                                               && !item.isStowed) as InventoryPartCase;
                            if (checkedCase != null)
                            {
                                print("case is present");
                                Port checkedPort = checkedCase.attachList.Find(item => item.portType == GameEnums.PortType.case_screw_mobo
                                                                               && item.occupiedByPartId == moboToCheck.partId);
                                if (checkedPort == null)
                                {
                                    print("good, mobo is not attached to case");
                                    // good to go, remove
                                    GameObject objectToRemove = GameObject.Find(moboToCheck.partId).transform.Find(partToDetach.partId).gameObject;
                                    if (objectToRemove != null)
                                    {
                                        print("removing object");
                                        Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_m2
                                                                                 && port.occupiedByPartId == partToDetach.partId);
                                        if (targetPort != null)
                                        {
                                            print("removing from slot and destroying");
                                            targetPort.occupiedByPartId = null;
                                            Destroy(objectToRemove);
                                            partToDetach.isStowed = true;
                                        }
                                    }
                                }
                                else
                                {
                                    // can't detach when mobo is attached to case!
                                    print("can't detach parts attached to mobo, remove mobo assembly from case first!");
                                }
                            }
                            else
                            {
                                // no case in world, remove ram
                                print("good, no case in world space");
                                // good to go, remove
                                GameObject objectToRemove = GameObject.Find(moboToCheck.partId).transform.Find(partToDetach.partId).gameObject;
                                if (objectToRemove != null)
                                {
                                    print("removing object");
                                    Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_m2
                                                                             && port.occupiedByPartId == partToDetach.partId);
                                    if (targetPort != null)
                                    {
                                        print("removing from slot and destroying");
                                        targetPort.occupiedByPartId = null;
                                        Destroy(objectToRemove);
                                        partToDetach.isStowed = true;
                                    }
                                }
                            }
                        }
                    } else if (partToDetach.partType == InventoryPartType.Motherboard)
                    {
                        InventoryPartCase caseToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
                        if (caseToCheck != null)
                        {
                            Port targetPort = caseToCheck.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_mobo
                                                                          && port.occupiedByPartId == partToDetach.partId);
                            if (targetPort != null)
                            {
                                // mobo attached to case
                                GameObject caseObject = GameObject.Find(caseToCheck.partId);
                                if (caseObject != null)
                                {
                                    if (((InventoryPartMotherboard)partToDetach).ports.Find(port => port.portType == GameEnums.PortType.mobo_pcieX16
                                                                                            && !string.IsNullOrEmpty(port.occupiedByPartId)) == null)
                                    {
                                        GameObject moboObject = caseObject.transform.Find(partToDetach.partId).gameObject;
                                        if (moboObject != null)
                                        {
                                            // detach motherboard assembly from case
                                            targetPort.occupiedByPartId = null;
                                            moboObject.transform.parent = caseObject.transform.parent;
                                            moboObject.transform.position = Transforms.pos_moboOnDesk;
                                            moboObject.transform.localEulerAngles = Transforms.rot_moboOnDesk;

                                            caseObject.transform.position = Transforms.pos_caseWorkingOnDesk;
                                            caseObject.transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;
                                        }
                                    } else
                                    {
                                        // can't detach motherboard from case when gpu is installed!
                                    }
                                }
                            } else
                            {
                                // mobo not attached to case
                                if (((InventoryPartMotherboard)partToDetach).ports.Find(port => !string.IsNullOrEmpty(port.occupiedByPartId)) == null)
                                {
                                    GameObject moboObject = GameObject.Find(partToDetach.partId);
                                    Destroy(moboObject);
                                    partToDetach.isStowed = true;
                                } else
                                {
                                    // can't remove motherboard if something is attached!
                                }
                            }
                        } else
                        {
                            // no case spawned
                            if (((InventoryPartMotherboard)partToDetach).ports.Find(port => !string.IsNullOrEmpty(port.occupiedByPartId)) == null)
                            {
                                GameObject moboObject = GameObject.Find(partToDetach.partId);
                                Destroy(moboObject);
                                partToDetach.isStowed = true;
                            }
                            else
                            {
                                // can't remove motherboard if something is attached!
                            }
                        }
                    } else if (partToDetach.partType == InventoryPartType.GPU)
                    {
                        InventoryPartCase caseToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
                        if (caseToCheck != null)
                        {
                            InventoryPartMotherboard
                                moboToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed)
                                as InventoryPartMotherboard;
                            if (moboToCheck != null)
                            {
                                Port targetAttachment = caseToCheck.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_mobo
                                                                                    && port.occupiedByPartId == moboToCheck.partId);
                                if (targetAttachment != null)
                                {
                                    // case is attached to mobo
                                    Port targetPort = moboToCheck.ports.Find(port => port.portType == GameEnums.PortType.mobo_pcieX16
                                                                             && port.occupiedByPartId == partToDetach.partId);
                                    if (targetPort != null)
                                    {
                                        // gpu is attached to mobo, remove
                                        GameObject caseObject = GameObject.Find(caseToCheck.partId);
                                        if (caseObject != null)
                                        {
                                            GameObject gpuObject = caseObject.transform.Find(partToDetach.partId).gameObject;
                                            if (gpuObject != null)
                                            {
                                                // finally remove
                                                targetPort.occupiedByPartId = null;
                                                Destroy(gpuObject);
                                                partToDetach.isStowed = true;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else if (partToDetach.partType == InventoryPartType.HDD)
                    {
                        InventoryPartCase caseToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
                        if (caseToCheck != null)
                        {
                            Port targetAttachment = caseToCheck.attachList.Find(port => port.portType == GameEnums.PortType.case_hddBay
                                                                                && port.occupiedByPartId == partToDetach.partId);
                            if (targetAttachment != null)
                            {
                                // remove
                                GameObject caseObject = GameObject.Find(caseToCheck.partId);
                                if (caseObject != null)
                                {
                                    GameObject hddObject = caseObject.transform.Find(partToDetach.partId).gameObject;
                                    if (hddObject != null)
                                    {
                                        // finally remove
                                        targetAttachment.occupiedByPartId = null;
                                        Destroy(hddObject);
                                        partToDetach.isStowed = true;
                                    }
                                }
                            }
                        } else
                        {
                            // can't remove because there is no case in the first place!
                        }
                    } else if (partToDetach.partType == InventoryPartType.PSU)
                    {
                        InventoryPartCase caseToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
                        if (caseToCheck != null)
                        {
                            Port targetAttachment = caseToCheck.attachList.Find(port => port.portType == GameEnums.PortType.case_psuBasement
                                                                                && port.occupiedByPartId == partToDetach.partId);
                            if (targetAttachment != null)
                            {
                                // remove
                                GameObject caseObject = GameObject.Find(caseToCheck.partId);
                                if (caseObject != null)
                                {
                                    GameObject psuObject = caseObject.transform.Find(partToDetach.partId).gameObject;
                                    if (psuObject != null)
                                    {
                                        // finally remove
                                        targetAttachment.occupiedByPartId = null;
                                        Destroy(psuObject);
                                        partToDetach.isStowed = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // can't remove because there is no case in the first place!
                        }
                    } else if (partToDetach.partType == InventoryPartType.SSDSata)
                    {
                        InventoryPartCase caseToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
                        if (caseToCheck != null)
                        {
                            Port targetAttachment = caseToCheck.attachList.Find(port => port.portType == GameEnums.PortType.case_ssdBay
                                                                                && port.occupiedByPartId == partToDetach.partId);
                            if (targetAttachment != null)
                            {
                                // remove
                                GameObject caseObject = GameObject.Find(caseToCheck.partId);
                                if (caseObject != null)
                                {
                                    GameObject ssdObject = caseObject.transform.Find(partToDetach.partId).gameObject;
                                    if (ssdObject != null)
                                    {
                                        // finally remove
                                        targetAttachment.occupiedByPartId = null;
                                        Destroy(ssdObject);
                                        partToDetach.isStowed = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // can't remove because there is no case in the first place!
                        }
                    } else if (partToDetach.partType == InventoryPartType.SystemFan)
                    {
                        InventoryPartCase caseToCheck = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) as InventoryPartCase;
                        if (caseToCheck != null)
                        {
                            Port targetAttachment = caseToCheck.attachList.Find(port => port.portType == GameEnums.PortType.case_screw_sysfan
                                                                                && port.occupiedByPartId == partToDetach.partId);
                            if (targetAttachment != null)
                            {
                                // remove
                                GameObject caseObject = GameObject.Find(caseToCheck.partId);
                                if (caseObject != null)
                                {
                                    GameObject fanObject = caseObject.transform.Find(partToDetach.partId).gameObject;
                                    if (fanObject != null)
                                    {
                                        // finally remove
                                        targetAttachment.occupiedByPartId = null;
                                        Destroy(fanObject);
                                        partToDetach.isStowed = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            // can't remove because there is no case in the first place!
                        }
                    }
                } else
                {
                    // can't detach something that is stowed!
                }
            }
        }

        selectedPartId = null;
        selectionModeEnabled = false;
        inventoryDisplay.UpdateDisplay();
    }

    public void PerformObjectiveSatisfactionChecks ()
    {
        // TODO handle objective satisfaction checks
        //     triggered by certain actions (part attachment, system variable changes, etc.)
        //     check objective type

        //     if MakeBootable
        //         check if built system is ready to boot
        //     if ReconfigureSystem
        //         check reconfiguration criteria if satisfied
        //     TimeLimit won't be checked here, it will be checked inside the Update() function instead
        //     if NoPartsBroken
        //         check if player did an oopsie (e.g. installing the CPU the wrong way)
        //     if MainObjective is satisfied, player can now end simulation

        foreach(Objective objective in currentScenario.scenarioObjectives)
        {
            if (objective.objectiveType == GameEnums.ObjectiveType.makeBootable)
            {
                if (scenarioItems.Find(item => item.damageType == GameEnums.DamageType.none && item.isStowed) == null
                    && scenarioItems.Find(item => !(item.damageType == GameEnums.DamageType.none) && !item.isStowed) == null)
                {
                    print("make bootable objective satisfied: true");
                    objective.isSatisfied = true;
                } else
                {
                    print("make bootable objective satisfied: false");
                    objective.isSatisfied = false;
                }
            } else if (objective.objectiveType == GameEnums.ObjectiveType.reconfigureSystem)
            {
                if (objective.configType == GameEnums.SystemConfigurationType.systemTempRatio)
                {
                    if (!isSystemOverheating)
                    {
                        objective.isSatisfied = true;
                    } else
                    {
                        objective.isSatisfied = false;
                    }
                }
                //else if (objective.configType == GameEnums.SystemConfigurationType.installOs)
                //{
                //    if (isOsInstalled)
                //    {
                //        objective.isSatisfied = true;
                //    } else
                //    {
                //        objective.isSatisfied = false;
                //    }
                //}
                else if (objective.configType == GameEnums.SystemConfigurationType.memoryProfile)
                {
                    if (isMemoryProfileChanged)
                    {
                        objective.isSatisfied = true;
                    } else
                    {
                        objective.isSatisfied = false;
                    }
                }
            }
            //else if (objective.objectiveType == GameEnums.ObjectiveType.noPartsBroken)
            //{
            //    if (partBroken)
            //    {
            //        objective.isSatisfied = false;
            //        FailSimulation();
            //    } else
            //    {
            //        objective.isSatisfied = true;
            //    }
            //}
        }
        Objective mainObjective = currentScenario.scenarioObjectives.Find(item => item.isMainObjective);

        if (scenarioItems.Find(item => item.damageType == GameEnums.DamageType.none && item.isStowed) == null
            && mainObjective.isSatisfied)
        {
            scenarioCleared = true;
            if (currentScenario.scenarioObjectives.Find(item => !item.isSatisfied) == null)
            {
                EndSimulation();
            }
        } else
        {
            scenarioCleared = false;
        }
        if (currentScenario.isTutorialScenario)
            objectivesPanelController.UpdateObjectiveDisplay();
    }

    public void EndSimulation ()
    {
        // show end screen
        timeLimit = 0;
        finalTimeMinutes = currentTimeMinutes;
        finalTimeSeconds = (int)currentTimeSeconds;
        GameObject.Find("UICanvas").SetActive(false);
        SuccessCanvas.SetActive(true);
        SuccessCanvas.GetComponent<SummaryCanvasController>().SetElapsedTime($"Elapsed time: {finalTimeMinutes}m {finalTimeSeconds}s");
    }

    public void FailSimulation ()
    {
        // show fail screen
        GameObject.Find("UICanvas").SetActive(false);
        FailCanvas.SetActive(true);
        FailCanvas.GetComponent<SummaryCanvasController>().SetElapsedTime($"Elapsed time: {timeLimit}m 0s");
    }

    public void RestartScene ()
    {
        SceneManager.LoadScene("TestScene");
    }

    public void ExitScene ()
    {
        SceneManager.LoadScene("TitleScreenScene");
    }

    public void EnableTestMode ()
    {
        List<InventoryPart> nonStowedParts = new();
        foreach (InventoryPart part in scenarioItems)
        {
            if (!part.isStowed)
            {
                nonStowedParts.Add(part);
            }
        }

        if (nonStowedParts.Find(item => item.partType == InventoryPartType.CPU && !item.isStowed) != null
            && nonStowedParts.Find(item => item.partType == InventoryPartType.Motherboard && !item.isStowed) != null
            && nonStowedParts.Find(item => item.partType == InventoryPartType.RAM && !item.isStowed) != null
            && nonStowedParts.Find(item => item.partType == InventoryPartType.CPUCooler && !item.isStowed) != null
            && nonStowedParts.Find(item => item.partType == InventoryPartType.PSU && !item.isStowed) != null
            && nonStowedParts.Find(item => item.partType == InventoryPartType.Case && !item.isStowed) != null
            && nonStowedParts.Find(item => item.partType == InventoryPartType.GPU && !item.isStowed) != null
            && nonStowedParts.Find(item => ((item.partType == InventoryPartType.SSDM2)
                                             || (item.partType == InventoryPartType.SSDSata) 
                                             || (item.partType == InventoryPartType.HDD))
                                    && !item.isStowed) != null)
        {
            // transition to test mode
            SwitchCameraView(GameEnums.CameraView.MonitorView);

            InventoryPart workingCase = nonStowedParts.Find(item => item.partType == InventoryPartType.Case);
            
            if (workingCase != null)
            {
                GameObject.Find(workingCase.partId).transform.position = Transforms.pos_caseWorkingOnDesk;
                GameObject.Find(workingCase.partId).transform.localEulerAngles = Transforms.rot_caseUprightOnDesk;
                monitor.SetActive(true);
                keyboard.SetActive(true);
                mouse.SetActive(true);
            }

            StartCoroutine(BootSequence());
        } else
        {
            // show error message
        }
    }

    IEnumerator BootSequence ()
    {
        GameObject.Find("UICanvas").GetComponent<CanvasGroup>().interactable = false;

        cpuPowerLimitDropdown.value = cpuPowerLimit;
        memoryProfileDropdown.ClearOptions();

        SetCpuPowerLimit();

        InventoryPart cpu = scenarioItems.Find(item => item.partType == InventoryPartType.CPU && !item.isStowed);
        List<InventoryPartRam> memoryModules = new();

        foreach(InventoryPart part in scenarioItems)
        {
            if (part.partType == InventoryPartType.RAM & !part.isStowed)
            {
                memoryModules.Add(part as InventoryPartRam);
            }
        }
        
        if (memoryModules.Count > 0 && cpu != null)
        {
            int memoryCapacity = 0;

            foreach (InventoryPartRam module in memoryModules)
            {
                memoryCapacity += module.size;
            }

            cpuInfo.GetComponent<TextMeshProUGUI>().text = $"{cpu.partName} @ {((InventoryPartCpu)cpu).baseFreq} MHz";
            memInfo.GetComponent<TextMeshProUGUI>().text = $"{memoryCapacity} MB @ {memorySpeed} MHz";

            List<TMP_Dropdown.OptionData> memoryProfileOptions = new();
            foreach(int xmpProfile in memoryModules[0].xmpFreqs)
            {
                memoryProfileOptions.Add(new TMP_Dropdown.OptionData($"Profile: {xmpProfile}MHz"));
            }
            memoryProfileDropdown.AddOptions(memoryProfileOptions);
        }


        yield return new WaitForSeconds(5);
        BootCanvas.SetActive(true);
        BootCanvas.transform.GetChild(1).gameObject.SetActive(true);
        BootCanvas.transform.GetChild(2).gameObject.SetActive(false);
        isBiosTriggerable = true;
        
        // during this time, user is free to hit the delete key

        yield return new WaitForSeconds(5);
        // disable delete key at this point
        isBiosTriggerable = false;
        
        if (isBiosTriggered)
        {
            // boot to bios here
            isBiosTriggered = false;
            BootCanvas.transform.GetChild(1).gameObject.SetActive(false);

            yield return new WaitForSeconds(2);
            // show bios screen
            BootCanvas.SetActive(false);
            BIOSCanvas.SetActive(true);

        } else
        {
            BootCanvas.transform.GetChild(1).gameObject.SetActive(false);
            yield return new WaitForSeconds(1);
            BootCanvas.transform.GetChild(2).gameObject.SetActive(true);

            yield return new WaitForSeconds(3);
            BootCanvas.transform.GetChild(2).gameObject.SetActive(false);
            // check if broken parts are installed
            if (scenarioItems.Find(item => !(item.damageType == GameEnums.DamageType.none)
                                   && !(item.partType == InventoryPartType.SystemFan || item.partType == InventoryPartType.CPUCooler)
                                   && !item.isStowed) != null)
            {
                // show error screen
                yield return new WaitForSeconds(1);
                BlueScreenCanvas.SetActive(true);
                yield return new WaitForSeconds(5);
                BlueScreenCanvas.SetActive(false);
                BootCanvas.SetActive(false);

                GameObject.Find("UICanvas").GetComponent<CanvasGroup>().interactable = true;

                InventoryPart workingCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed);
                if (workingCase != null)
                    GameObject.Find(workingCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;

                GameObject.Find("monitor").SetActive(false);
                GameObject.Find("keyboard").SetActive(false);
                GameObject.Find("mouse").SetActive(false);

                SwitchCameraView(GameEnums.CameraView.DefaultTableView);

            } else
            {
                yield return new WaitForSeconds(2);
                BootCanvas.SetActive(false);
                ScreenViewCanvas.SetActive(true);
            }
            PerformObjectiveSatisfactionChecks();
        }
    }

    IEnumerator ShutdownSequence ()
    {
        ScreenViewCanvas.SetActive(false);
        StressTestCanvas.SetActive(false);

        BootCanvas.SetActive(true);
        BootCanvas.transform.GetChild(1).gameObject.SetActive(false);
        BootCanvas.transform.GetChild(2).gameObject.SetActive(false);

        yield return new WaitForSeconds(3);
        BootCanvas.transform.GetChild(2).gameObject.SetActive(true);
        
        yield return new WaitForSeconds(7);
        BootCanvas.transform.GetChild(2).gameObject.SetActive(false);
        BootCanvas.SetActive(false);

        yield return new WaitForSeconds(2);
        GameObject.Find("UICanvas").GetComponent<CanvasGroup>().interactable = true;

        InventoryPart workingCase = scenarioItems.Find(item => item.partType == InventoryPartType.Case && !item.isStowed);
        if (workingCase != null)
            GameObject.Find(workingCase.partId).transform.position = Transforms.pos_caseUprightOnDesk;

        GameObject.Find("monitor").SetActive(false);
        GameObject.Find("keyboard").SetActive(false);
        GameObject.Find("mouse").SetActive(false);

        SwitchCameraView(GameEnums.CameraView.DefaultTableView);
    }

    IEnumerator BiosShutdownSequence ()
    {
        yield return new WaitForSeconds(2);
        BIOSCanvas.SetActive(false);
        StartCoroutine(BootSequence());
    }

    public void StartStressTest()
    {
        StartCoroutine(StressTestSequence());
    }

    IEnumerator StressTestSequence ()
    {
        StressTestCanvas.transform.GetChild(0).GetChild(2).gameObject.GetComponent<Button>().interactable = false;
        StressTestCanvas.transform.GetChild(0).GetChild(4).gameObject.GetComponent<TextMeshProUGUI>().text = "Stressing system...";

        yield return new WaitForSeconds(1);
        InventoryPartCpuCooler
            cooler = scenarioItems.Find(item => item.partType == InventoryPartType.CPUCooler && !item.isStowed)
            as InventoryPartCpuCooler;
        float tdpRatio = ((float)cooler.maxTdp / (float)cpuTdp) * 100;
        float overheatChance = CalculateOverheatingChance((int)tdpRatio);
        System.Random rand = new System.Random();
        int chance = rand.Next(1, 101);

        print($"cooler tdp: {cooler.maxTdp}, cpu tdp: {cpuTdp}, tdpRatio: {tdpRatio}, overheat chance: {overheatChance}, chance: {chance}");
        if (chance <= overheatChance)
        {
            print("system overheating!");
            StressTestCanvas.transform.GetChild(0).GetChild(4).gameObject.GetComponent<TextMeshProUGUI>().text = "Overheating!";
            isSystemOverheating = true;
        }
        else
        {
            print("system is fine");
            StressTestCanvas.transform.GetChild(0).GetChild(4).gameObject.GetComponent<TextMeshProUGUI>().text = "Temps OK!";
            isSystemOverheating = false;
        }
        StressTestCanvas.transform.GetChild(0).GetChild(4).gameObject.GetComponent<TextMeshProUGUI>().text += $"\n{(((105 - 85) * (chance - 100)) / (1 - 100)) + 85} C";
        yield return new WaitForSeconds(3);

        StressTestCanvas.transform.GetChild(0).GetChild(2).gameObject.GetComponent<Button>().interactable = true;
        StressTestCanvas.transform.GetChild(0).GetChild(4).gameObject.GetComponent<TextMeshProUGUI>().text = "Waiting for test";

        yield return new WaitForSeconds(2);
        PerformObjectiveSatisfactionChecks();
    }

    public void ExitTestMode ()
    {
        StartCoroutine(ShutdownSequence());
    }

    public void ExitBios ()
    {
        StartCoroutine(BiosShutdownSequence());
    }

    public void SetMemoryProfile()
    {
        InventoryPartRam
            memory = scenarioItems.Find(item => item.partType == InventoryPartType.RAM && !item.isStowed)
            as InventoryPartRam;
        if (memory != null)
        {
            if (memory.xmpFreqs[memoryProfileDropdown.value] > memory.xmpFreqs[0])
            {
                isMemoryProfileChanged = true;
            } else
            {
                isMemoryProfileChanged = false;
            }
            memorySpeed = memory.xmpFreqs[memoryProfileDropdown.value];
        }
    }

    public void SetCpuPowerLimit ()
    {
        cpuPowerLimit = cpuPowerLimitDropdown.value;
        InventoryPartCpu cpu = scenarioItems.Find(item => item.partType == InventoryPartType.CPU && !item.isStowed) as InventoryPartCpu;
        if (cpuPowerLimit == 0)
            cpuTdp = (int)(cpu.tdp * 0.75);
        else if (cpuPowerLimit == 1)
            cpuTdp = (int)(cpu.tdp * 0.8);
        else if (cpuPowerLimit == 2)
            cpuTdp = (int)(cpu.tdp * 1);
    }

    public float CalculateOverheatingChance (int tdpRatio)
    {
        if (tdpRatio > 150)
            return 5;
        else if (tdpRatio <= 150 && tdpRatio >= 50)
            return (5 - 75) * (tdpRatio - 50) / (150 - 50) + 75;
        else if (tdpRatio < 50)
            return 75;
        else
            return 75;
    }

    public void LoadScenario ()
    {
        string folder = Application.dataPath + "/LevelData/";
        string scenarioFileName = GlobalPlayerData.Instance.sceneToLoad + ".json";
        string path = Path.Combine(folder, scenarioFileName);

        if (File.Exists(path))
        {
            JObject scenarioJson = JObject.Parse(File.ReadAllText(path));
            currentScenario = new();

            currentScenario.scenarioName = scenarioJson["scenarioName"].ToString();
            currentScenario.scenarioTitle = scenarioJson["scenarioTitle"].ToString();
            currentScenario.scenarioDescription = scenarioJson["scenarioDescription"].ToString();
            currentScenario.isTutorialScenario = scenarioJson["isTutorialScenario"].ToObject<bool>();

            currentScenario.scenarioObjectives = new();
            IList<JToken> tempObjectives = scenarioJson["scenarioObjectives"].Children().ToList();
            foreach (JToken tempObjective in tempObjectives)
            {
                Objective objective = tempObjective.ToObject<Objective>();
                currentScenario.scenarioObjectives.Add(objective);
            }

            currentScenario.scenarioUUID = scenarioJson["scenarioUUID"].ToString();

            currentScenario.inventoryParts = new();
            IList<JToken> tempInventoryParts = scenarioJson["inventoryParts"].Children().ToList();
            foreach (JToken tempInventoryPart in tempInventoryParts)
            {
                Debug.Log(tempInventoryPart["partType"]);
                if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.Case)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartCase>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.CPU)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartCpu>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.CPUCooler)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartCpuCooler>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.GPU)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartGpu>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.HDD)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartHdd>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.Motherboard)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartMotherboard>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.PSU)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartPsu>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.RAM)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartRam>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.SSDM2)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartSsdMTwo>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.SSDSata)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartSsdSata>());
                }
                else if (tempInventoryPart["partType"].ToObject<int>() == (int)InventoryPartType.SystemFan)
                {
                    currentScenario.inventoryParts.Add(tempInventoryPart.ToObject<InventoryPartSystemFan>());
                }
                else
                {
                    Debug.Log("Unknown inventory part detected - maybe the exported JSON got tampered.");
                }
            }
        } else
        {
            Debug.Log($"{path} not found");
        }
    }

    public void GenerateInventory()
    {
        // add from Scenario object here
        scenarioItems = currentScenario.inventoryParts;

        // special check for stowed case
        InventoryPartCase
            checkedCase = scenarioItems.Find(item => item.GetType() == typeof(InventoryPartCase))
            as InventoryPartCase;
        if (checkedCase != null)
        {
            if (checkedCase.attachList.Find(attachment => !string.IsNullOrEmpty(attachment.occupiedByPartId)) == null)
            {
                checkedCase.isStowed = true;
            }
        }

        // update display
        inventoryDisplay.UpdateDisplay();
    }

    public void StowItem(string partId)
    {
        // stowing item is result of removal of item from system
        //     do the following:
        //     check if item is in the current scenario's inventory
        //     remove then update display if present
        //     otherwise fail
        InventoryPart partToStow = CheckCurrentInventory(partId);
        if (partToStow != null)
        {
            partToStow.isStowed = true;
            inventoryDisplay.UpdateDisplay();
        }
    }

    public void UnstowItem(string partId)
    {
        // unstowing item is result of attachment of item to part
        InventoryPart partToUnstow = CheckCurrentInventory(partId);
        if (partToUnstow != null)
        {
            partToUnstow.isStowed = false;
            inventoryDisplay.UpdateDisplay();
        }
    }

    public InventoryPart CheckCurrentInventory(string partId)
    {
        return scenarioItems.Find(item => item.partId == partId);
    }

    public void ShowGpuAttachmentPopup (InventoryPart selectedPart)
    {
        var obj = Instantiate(Resources.Load<GameObject>("UIPrefabs/GpuAttachmentCanvas"), transform.parent);
        obj.GetComponent<GpuAttachmentCanvasController>().selectedPart = selectedPart;
    }

    public void ShowRamAttachmentPopup (InventoryPart selectedPart)
    {
        var obj = Instantiate(Resources.Load<GameObject>("UIPrefabs/RamAttachmentCanvas"), transform.parent);
        obj.GetComponent<RamAttachmentCanvasController>().selectedPart = selectedPart;
    }

    public void ShowSystemFanAttachmentPopup (InventoryPart selectedPart)
    {
        var obj = Instantiate(Resources.Load<GameObject>("UIPrefabs/SystemFanAttachmentCanvas"), transform.parent);
        obj.GetComponent<SystemFanAttachmentCanvasController>().selectedPart = selectedPart;
    }

}
