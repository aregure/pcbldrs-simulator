using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectiveListItemController : MonoBehaviour
{
    private GameObject dataManager;
    public Objective objective;

    private void Awake()
    {
        dataManager = GameObject.Find("DataManager");
    }

    public void SelectListItem()
    {
        dataManager.GetComponent<DataManagerController>().SelectObjectiveFromList(objective);
        gameObject.transform.GetChild(2).GetComponent<Toggle>().isOn = true;
    }
}
