using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycastController : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    private GameObject gameManager;
    private GameObject partRemovalCanvas;

    private void Awake()
    {
        gameManager = GameObject.Find("GameManager");
    }

    private void Update()
    {
        if (gameManager.GetComponent<GameManagerController>().selectionModeEnabled)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                var ray = _camera.ScreenPointToRay(Input.mousePosition);
                Debug.DrawRay(ray.origin, ray.direction * 100f, Color.red, 1f);
                if (Physics.Raycast(ray, out var hitInfo))
                {
                    GameObject hitObject = hitInfo.transform.gameObject;
                    var prefabPartIdController = hitObject.GetComponentInParent<PrefabPartIdController>();
                    if (prefabPartIdController != null)
                    {
                        Debug.Log($"clicked {prefabPartIdController.partId}");
                        gameManager.GetComponent<GameManagerController>().selectedPartId = prefabPartIdController.partId;
                        gameManager.GetComponent<GameManagerController>().selectionModeEnabled = false;
                    }
                    partRemovalCanvas = GameObject.Find("PartRemovalCanvas(Clone)");
                    if (partRemovalCanvas != null)
                    {
                        partRemovalCanvas.GetComponent<PartRemovalCanvasController>()
                            .SetPartName(gameManager.GetComponent<GameManagerController>()
                            .scenarioItems.Find(item => item.partId == prefabPartIdController.partId).partName);
                    }
                }
            }
        }
    }

}
