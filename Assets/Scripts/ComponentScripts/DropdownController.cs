using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DropdownController : MonoBehaviour
{
    private GameObject dataManager;
    public void PopulatePorts ()
    {
        dataManager = GameObject.Find("DataManager");
        TMP_Dropdown dropdown = GetComponent<TMP_Dropdown>();
        Debug.Log($"Value changed! Value is now {dropdown.value}");
        dataManager.GetComponent<DataManagerController>().LoadPorts(dropdown.value - 1);
    }
}
