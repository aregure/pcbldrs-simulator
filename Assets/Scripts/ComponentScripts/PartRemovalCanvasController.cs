using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PartRemovalCanvasController : MonoBehaviour
{
    public GameObject sourceCanvas;
    public GameObject destCanvas;
    private GameObject gameManager;

    private void Awake()
    {
        sourceCanvas = transform.gameObject;
        gameManager = GameObject.Find("GameManager");

        GameObject[] objects = FindObjectsOfType<GameObject>();
        foreach (GameObject gameObject in objects)
        {
            if (gameObject.GetComponent<CanvasGroup>())
            {
                destCanvas = gameObject;
            }
        }
        destCanvas.GetComponent<CanvasGroup>().interactable = false;
    }

    public void CloseCanvas()
    {
        gameManager.GetComponent<GameManagerController>().selectionModeEnabled = false;
        gameManager.GetComponent<GameManagerController>().selectedPartId = null;
        destCanvas.GetComponent<CanvasGroup>().interactable = true;
        Destroy(sourceCanvas);
    }

    public void InitiateRemoval ()
    {
        gameManager = GameObject.Find("GameManager");
        gameManager.GetComponent<GameManagerController>().DetachSelectedPart();
    }

    public void SetPartName (string name)
    {
        transform.GetChild(0).GetChild(0).gameObject.GetComponent<TextMeshProUGUI>().text = name;
    }

}
