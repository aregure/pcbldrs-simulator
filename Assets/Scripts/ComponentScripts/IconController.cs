using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconController : MonoBehaviour
{
    public string partId;
    public GameObject GameManager;
    private GameObject UICanvas;

    private void Awake()
    {
        GameManager = GameObject.Find("GameManager");
        UICanvas = GameObject.Find("UICanvas");
    }

    public void SelectPartForAttachment()
    {
        GameManager.GetComponent<GameManagerController>().SelectPartFromInventoryDisplay(partId);
    }

    public void CloseInventoryCanvas()
    {
        UICanvas.GetComponent<CanvasGroup>().interactable = true;
        GameObject.Find("InventoryCanvas").SetActive(false);
    }
}
