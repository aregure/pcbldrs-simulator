using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PopupCanvasController : MonoBehaviour
{
    public GameObject sourceCanvas;
    public GameObject destCanvas;
    private GameObject headerTextBox;
    private GameObject bodyTextBox;
    private GameObject buttonTextBox;

    private void Awake()
    {
        sourceCanvas = transform.gameObject;
        headerTextBox = transform.GetChild(0).GetChild(1).GetChild(0).GetChild(0).gameObject;
        bodyTextBox = transform.GetChild(0).GetChild(1).GetChild(1).GetChild(0).gameObject;
        buttonTextBox = transform.GetChild(0).GetChild(1).GetChild(1).GetChild(1).GetChild(0).gameObject;

        GameObject[] objects = FindObjectsOfType<GameObject>();
        foreach (GameObject gameObject in objects)
        {
            if (gameObject.GetComponent<CanvasGroup>())
            {
                destCanvas = gameObject;
            }
        }
        destCanvas.GetComponent<CanvasGroup>().interactable = false;
    }

    public void SetHeaderText (string text)
    {
        headerTextBox.GetComponent<TextMeshProUGUI>().text = text;
    }

    public void SetBodyText (string text)
    {
        bodyTextBox.GetComponent<TextMeshProUGUI>().text = text;
    }

    public void SetButtonText (string text)
    {
        buttonTextBox.GetComponent<TextMeshProUGUI>().text = text;
    }

    public void CloseCanvas()
    {
        destCanvas.GetComponent<CanvasGroup>().interactable = true;
        Destroy(sourceCanvas);
    }

}
