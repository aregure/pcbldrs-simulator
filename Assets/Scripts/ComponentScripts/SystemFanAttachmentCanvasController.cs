using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemFanAttachmentCanvasController : MonoBehaviour
{
    public GameObject sourceCanvas;
    public GameObject destCanvas;
    private GameObject gameManager;
    public InventoryPart selectedPart;

    private void Awake()
    {
        sourceCanvas = transform.gameObject;
        gameManager = GameObject.Find("GameManager");

        GameObject[] objects = FindObjectsOfType<GameObject>();
        foreach (GameObject gameObject in objects)
        {
            if (gameObject.GetComponent<CanvasGroup>())
            {
                destCanvas = gameObject;
            }
        }
        destCanvas.GetComponent<CanvasGroup>().interactable = false;
    }

    public void CloseCanvas()
    {
        destCanvas.GetComponent<CanvasGroup>().interactable = true;
        Destroy(sourceCanvas);
    }

    public void AttachFanFront1()
    {
        gameManager.GetComponent<GameManagerController>().AttachSystemFan(selectedPart, "front1");
    }

    public void AttachFanFront2()
    {
        gameManager.GetComponent<GameManagerController>().AttachSystemFan(selectedPart, "front2");
    }

    public void AttachFanFront3()
    {
        gameManager.GetComponent<GameManagerController>().AttachSystemFan(selectedPart, "front3");
    }

    public void AttachFanTop1()
    {
        gameManager.GetComponent<GameManagerController>().AttachSystemFan(selectedPart, "top1");
    }

    public void AttachFanTop2()
    {
        gameManager.GetComponent<GameManagerController>().AttachSystemFan(selectedPart, "top2");
    }

    public void AttachFanRear()
    {
        gameManager.GetComponent<GameManagerController>().AttachSystemFan(selectedPart, "rear");
    }
}
