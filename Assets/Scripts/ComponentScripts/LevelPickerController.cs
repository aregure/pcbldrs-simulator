using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json.Linq;
using TMPro;
using System.Linq;

public class LevelPickerController : MonoBehaviour
{
    [SerializeField] private List<Scenario> customScenarios;
    public GameObject customScenarioListView;
    public GameObject standardScenarioListView;

    public void LoadCustomScenarios ()
    {
        if (customScenarioListView.transform.childCount > 0)
        {
            for (int i = 0; i < customScenarioListView.transform.childCount; i++)
            {
                Destroy(customScenarioListView.transform.GetChild(i).gameObject);
            }
        }

        string folder = Application.dataPath + "/LevelData/CustomScenarios/";

        if (!Directory.Exists(folder))
            Directory.CreateDirectory(folder);

        DirectoryInfo directoryInfo = new DirectoryInfo(folder);

        foreach (var file in directoryInfo.GetFiles("*.json"))
        {
            string path = Path.Combine(folder, file.Name);
            JObject scenarioJson = JObject.Parse(File.ReadAllText(path));

            var obj = Instantiate(Resources.Load<GameObject>("UIPrefabs/CustomScenarioListItem"), customScenarioListView.transform);
            obj.GetComponent<CustomScenarioListItemController>().scenarioFileName = scenarioJson["scenarioName"].ToString();
            obj.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = scenarioJson["scenarioTitle"].ToString();
            obj.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = scenarioJson["scenarioName"].ToString();
            obj.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = scenarioJson["scenarioDescription"].ToString();
            obj.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = scenarioJson["isTutorialScenario"].ToObject<bool>() ? "Tutorial scenario: Yes" : "Tutorial scenario: No";
            obj.transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = "No time limit";
            
            IList<JToken> objectiveList = scenarioJson["scenarioObjectives"].Children().ToList();
            foreach (JToken token in objectiveList)
            {
                Objective objective = token.ToObject<Objective>();
                if (objective.objectiveType == GameEnums.ObjectiveType.timeLimit)
                    obj.transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = $"Time limit: {objective.objectiveTimeLimit} minutes";
            }
        }
    }

    public void LoadStandardScenarios()
    {
        if (standardScenarioListView.transform.childCount > 0)
        {
            for (int i = 0; i < standardScenarioListView.transform.childCount; i++)
            {
                Destroy(standardScenarioListView.transform.GetChild(i).gameObject);
            }
        }

        string folder = Application.dataPath + "/LevelData/StandardScenarios/";

        if (!Directory.Exists(folder))
            Directory.CreateDirectory(folder);

        DirectoryInfo directoryInfo = new DirectoryInfo(folder);

        foreach (var file in directoryInfo.GetFiles("*.json"))
        {
            string path = Path.Combine(folder, file.Name);
            JObject scenarioJson = JObject.Parse(File.ReadAllText(path));

            var obj = Instantiate(Resources.Load<GameObject>("UIPrefabs/StandardScenarioListItem"), standardScenarioListView.transform);
            obj.GetComponent<CustomScenarioListItemController>().scenarioFileName = scenarioJson["scenarioName"].ToString();
            obj.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = scenarioJson["scenarioTitle"].ToString();
            obj.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = scenarioJson["scenarioName"].ToString();
            obj.transform.GetChild(4).GetComponent<TextMeshProUGUI>().text = scenarioJson["scenarioDescription"].ToString();
            obj.transform.GetChild(5).GetComponent<TextMeshProUGUI>().text = scenarioJson["isTutorialScenario"].ToObject<bool>() ? "Tutorial scenario: Yes" : "Tutorial scenario: No";
            obj.transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = "No time limit";

            IList<JToken> objectiveList = scenarioJson["scenarioObjectives"].Children().ToList();
            foreach (JToken token in objectiveList)
            {
                Objective objective = token.ToObject<Objective>();
                if (objective.objectiveType == GameEnums.ObjectiveType.timeLimit)
                    obj.transform.GetChild(6).GetComponent<TextMeshProUGUI>().text = $"Time limit: {objective.objectiveTimeLimit} minutes";
            }
        }
    }
}
