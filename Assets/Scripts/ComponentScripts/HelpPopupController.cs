using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpPopupController : MonoBehaviour
{
    public void ShowPopup(string headerText = "Header",
                          string bodyText = "Body",
                          string buttonText = "OK")
    {
        var obj = Instantiate(Resources.Load<GameObject>("UIPrefabs/PopupCanvas"), null);
        obj.GetComponent<PopupCanvasController>().SetHeaderText(headerText);
        obj.GetComponent<PopupCanvasController>().SetBodyText(bodyText);
        obj.GetComponent<PopupCanvasController>().SetButtonText(buttonText);
    }

    public void ShowLearnPopup()
    {
        ShowPopup("Help - Learn menu",
                  "Click and drag with the mouse or use the scroll wheel to scroll across the page.",
                  "OK");
    }

    public void ShowPlayPopup()
    {
        ShowPopup("Help - Play menu",
                  "Standard scenario mode lets you play built-in scenarios.\n" +
                  "Custom scenario mode lets you play custom-made scenarios.",
                  "OK");
    }

    public void ShowCustomScenarioPopup()
    {
        ShowPopup("Help - Custom Scenario menu",
                  "Select a custom scenario to play.\n" +
                  "To create a custom scenario, click the \"Custom scenario creator\" button.",
                  "OK");
    }
    
    public void ShowStandardScenarioPopup()
    {
        ShowPopup("Help - Standard Scenario menu",
                  "Select a standard scenario to play.",
                  "OK");
    }

    public void ShowCustomScenarioCreatorPopup()
    {
        ShowPopup("Help - Custom Scenario Creator",
                  "Create a custom scenario.\n" +
                  "Provide a file name, title, objectives, and parts included, to create and export a scenario.",
                  "OK");
    }
}
