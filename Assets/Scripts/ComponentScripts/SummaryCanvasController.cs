using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SummaryCanvasController : MonoBehaviour
{
    public GameObject elapsedTime;

    public void SetElapsedTime (string time)
    {
        elapsedTime.GetComponent<TextMeshProUGUI>().text = time;
    }

}
