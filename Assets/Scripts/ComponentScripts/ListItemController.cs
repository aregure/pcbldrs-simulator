using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ListItemController : MonoBehaviour
{
    private GameObject dataManager;
    public InventoryPart inventoryPart;

    private void Awake()
    {
        dataManager = GameObject.Find("DataManager");
    }

    public void SelectListItem ()
    {
        dataManager.GetComponent<DataManagerController>().SelectPartFromPartList(inventoryPart);
        gameObject.transform.GetChild(3).GetComponent<Toggle>().isOn = true;
    }
}
