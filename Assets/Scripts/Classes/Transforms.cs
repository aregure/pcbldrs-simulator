using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transforms
{
    // cpu transforms, mobo desk
    public static Vector3 pos_cpuMountedOnMobo = new Vector3(0.0485f, 6.13f, -0.6708f);
    public static Vector3 pos_cpuFloatingOnMobo = new Vector3(0.0485f, 6.445f, -0.6708f);
    public static Vector3 rot_cpuMountedOnMobo = new Vector3(-90f, -90f, 0);
    public static Vector3 rot_cpuFloatingOnMobo = new Vector3(-90f, -90f, 180f);

    // cpu cooler transforms, mobo desk
    public static Vector3 pos_cpuCoolerMountedOnMobo = new Vector3(0.049f, 6.442f, -0.669f);
    public static Vector3 pos_cpuCoolerFloatingOnMobo = new Vector3(0.049f, 6.85f, -0.669f);
    public static Vector3 rot_cpuCoolerOnMobo = new Vector3(0f, 180f, 0f);

    // dimm transforms, mobo desk
    public static Vector3 pos_dimm1MountedOnMobo = new Vector3(0.5109f, 6.259f, -0.698f);
    public static Vector3 pos_dimm2MountedOnMobo = new Vector3(0.58001f, 6.259f, -0.698f);
    public static Vector3 pos_dimm3MountedOnMobo = new Vector3(0.65001f, 6.259f, -0.698f);
    public static Vector3 pos_dimm4MountedOnMobo = new Vector3(0.72101f, 6.259f, -0.698f);
    public static Vector3 pos_dimm1FloatingOnMobo = new Vector3(0.5109f, 6.259f, -0.698f);
    public static Vector3 pos_dimm2FloatingOnMobo = new Vector3(0.58001f, 6.259f, -0.698f);
    public static Vector3 pos_dimm3FloatingOnMobo = new Vector3(0.65001f, 6.259f, -0.698f);
    public static Vector3 pos_dimm4FloatingOnMobo = new Vector3(0.72101f, 6.259f, -0.698f);
    public static Vector3 rot_dimmOnMobo = new Vector3(-90f, 180f, 0f);

    // m2 transforms, mobo desk
    public static Vector3 pos_m2MountedOnMobo = new Vector3(-0.107f, 6.1487f, -1.531f);
    public static Vector3 pos_m2FloatingOnMobo = new Vector3(-0.2f, 6.1487f, -1.531f);
    public static Vector3 rot_m2OnMobo = new Vector3(-90f, 0f, 180f);

    // gpu transforms, case laid
    public static Vector3 pos_gpuMountedOnMoboTop = new Vector3(-0.288f, 6.392f, -0.325f);
    public static Vector3 pos_gpuMountedOnMoboBottom = new Vector3(-0.288f, 6.392f, -0.813f);
    public static Vector3 pos_gpuFloatingOnMoboTop = new Vector3(-0.288f, 6.541f, -0.325f);
    public static Vector3 pos_gpuFloatingOnMoboBottom = new Vector3(-0.288f, 6.541f, -0.813f);
    public static Vector3 rot_gpuOnMobo = new Vector3(0f, 180f, 0f);

    // case fan transforms, case upright
    public static Vector3 pos_systemFanOnCaseFront1 = new Vector3(1.901f, 8.769f, -0.615f);
    public static Vector3 pos_systemFanOnCaseFront2 = new Vector3(1.901f, 7.769f, -0.615f);
    public static Vector3 pos_systemFanOnCaseFront3 = new Vector3(1.901f, 6.769f, -0.615f);
    public static Vector3 pos_systemFanOnCaseTop1 = new Vector3(0.727f, 9.328f, -0.82f);
    public static Vector3 pos_systemFanOnCaseTop2 = new Vector3(-0.273f, 9.328f, -0.82f);
    public static Vector3 pos_systemFanOnCaseRear = new Vector3(-0.9f, 8.539f, -0.87f);
    public static Vector3 rot_systemFanOnCaseFront = new Vector3(0f, 180f, 0f);
    public static Vector3 rot_systemFanOnCaseTop = new Vector3(0f, 0f, 90f);
    public static Vector3 rot_systemFanOnCaseRear = new Vector3(0f, 180f, 0f);

    // psu transforms, case upright
    public static Vector3 pos_psuOnCase = new Vector3(-0.69f, 6.592f, -0.59f);
    public static Vector3 rot_psuOnCase = new Vector3(-90f, 0f, 180f);

    // sata drive transforms, case upright
    public static Vector3 pos_hddOnCase = new Vector3(0.751f, 6.262f, -0.739f);
    public static Vector3 pos_ssdSataOnCase = new Vector3(1.465f, 6.147f, -0.546f);
    public static Vector3 rot_sataOnCase = new Vector3(-90f, 0f, 0f);

    // io shield transforms, case laid
    public static Vector3 pos_ioShieldOnCase = new Vector3(-1f, 6.4866f, 0.329f);
    public static Vector3 rot_ioShieldOnCase = new Vector3(-90f, 180f, 0f);

    // motherboard transforms
    public static Vector3 pos_moboOnDesk = new Vector3(0f, 6.091f, -1f);
    public static Vector3 pos_moboOnDeskSetAside = new Vector3(4f, 6.091f, -1f);
    public static Vector3 pos_moboFloatingOnCase = new Vector3(0f, 8f, 0f);
    public static Vector3 pos_moboMountedOnCase = new Vector3(0f, 6.341f, 0f);
    public static Vector3 rot_moboOnDesk = new Vector3(0f, 180f, 0f);
    public static Vector3 rot_moboCluster = new Vector3(0f, 0f, 0f);

    // case transforms
    public static Vector3 pos_caseUprightOnDesk = new Vector3(0f, 8.049f, 0f);
    public static Vector3 pos_caseLaidOnDesk = new Vector3(0f, 6.281f, 0f);
    public static Vector3 pos_caseWorkingOnDesk = new Vector3(1f, 8.049f, 2f);
    public static Vector3 rot_caseLaidOnDesk = new Vector3(0f, 180f, 0f);
    public static Vector3 rot_caseUprightOnDesk = new Vector3(90f, 180f, 0f);

    // dimm latch rotations
    public static Vector3 rot_dimmOpen = new Vector3(-90f, 0f, 0f);
    public static Vector3 rot_dimmClosed = new Vector3(-45f, 0f, 0f);

    // bmd cpu retention arm rotations
    public static Vector3 rot_cpuArmBmdOpen = new Vector3(-90f, 90f, -90f);
    public static Vector3 rot_cpuArmBmdClosed = new Vector3(0f, 90f, -90f);



}
