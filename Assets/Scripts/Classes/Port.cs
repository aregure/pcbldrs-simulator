using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Port
{
    public string portName;
    public GameEnums.PortType portType;
    public string occupiedByPartId;
}
