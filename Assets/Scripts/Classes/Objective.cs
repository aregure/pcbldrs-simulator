using UnityEngine;

[System.Serializable]
public class Objective
{
    public GameEnums.ObjectiveType objectiveType;               // type of objective
    public bool isMainObjective;                                // main objective flag
                                                                //     simulation can be ended if objective is satisfied
                                                                //     sub objectives are considered bonus
    public bool isFailable;                                     // simulation fails immediately if objective is not met

    public GameEnums.SystemConfigurationType configType;        // type of configuration to do, used with reconfigureSystem objective type
    public int objectiveTimeLimit;                              // amount of time for time limit, used with timeLimit objectiveType

    public bool isSatisfied;                                    // flag for determining if objective is satisfied
}
