using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnums
{
    public enum CameraView
    {
        DefaultTableView,
        OverheadView,
        CpuAttachmentView,
        RamAttachmentView,
        CoolerAttachmentView,
        m2AttachmentView,
        IoShieldAttachmentView,
        MoboAttachmentView,
        CaseFanFrontAttachmentView,
        CaseFanTopAttachmentView,
        PsuAttachmentView,
        SataStorageAttachmentView,
        MonitorView,
        DefaultTableView_rear,
        DefaultTableView_left,
        DefaultTableView_right
    }

    public enum ObjectiveType
    {
        makeBootable,                   // assemble working parts and get system to BIOS-bootable state
                                        //     no working parts must be left stowed except for coolers
                                        //     no broken parts must be connected
                                        //     all dirty parts must be cleaned and connected
                                        //     system must be properly working

        reconfigureSystem,              // set BIOS to correct settings

        timeLimit                       // time limit

        // noPartsBroken                // no parts broken, insta-fail simulation if not met
    }

    public enum SystemConfigurationType
    {
        systemTempRatio,                // lower CPU temperature to a non-overheating level
                                        //     use this scenario for overheating simulations

        memoryProfile,                  // adjust memory XMP profiles to higher than defaults

        // installOs                    // install a fresh operating system to the computer
    }

    public enum SocketType
    {
        fm2,
        am3,
        am4,
        lga1150,
        lga1200
    }

    public enum DamageType
    {
        none,
        loose,
        broken,
        dirty
    }

    public enum PortType
    {
        mobo_cpu,                               // CPU socket
        mobo_dram,                              // DIMM slots, ranging from 0 to 3 (1 to 4 in normie indexes)
        mobo_pcieX16,                           // PCIe x16 slot, ranging from 0 to 1 (1 to 2)
        mobo_pcie,                              // PCIe x4 slot
        mobo_atx_24Pin,                         // 24-pin power connector
        mobo_atx_cpuPower,                      // 24-pin CPU power connector
        mobo_sata,                              // SATA header, ranging from 0 to 5 (1 to 6)
        mobo_m2,                                // M.2 slot
        mobo_frontAudio,                        // front panel audio
        mobo_pwrReset,                          // power and reset switch
        mobo_usbHeader,                         // USB 2.0 header for case front panel USB
        mobo_usb,                               // USB ports on motherboard I/O
        mobo_cpuFan,                            // CPU fan header
        mobo_cpuCoolerBracket,                  // motherboard's CPU cooler backplate
        mobo_sysFan,                            // system fan header, ranging from 0 to 3 (1 to 4)
        mobo_argb,                              // ARGB header, ranging from 0 to 1 (1 to 2)
        mobo_hdmi,                              // HDMI port on motherboard I/O
        mobo_vga,                               // VGA port on motherboard I/O
        gpu_pwr_6pin,                           // PCIe power connector (6-pin)
        gpu_pwr_8pin,                           // PCIe power connector (8-pin)
        gpu_dp,                                 // DisplayPort connector on GPU I/O
        gpu_hdmi,                               // HDMI connector on GPU I/O
        gpu_vga,                                // VGA connector on GPU I/O
        psu_sataPwr,                            // SATA power cable for non-M.2 drives
        case_screw_sysfan,                      // for parts that connect to chassis (e.g. case fans)
        case_screw_mobo,                        // motherboard screwing area
        case_hddBay,                            // for 3.5" hard drives
        case_ssdBay,							// for 2.5" SSDs
        case_psuBasement                        // PSU basement
    }

    public enum CaseSpec
    {
        atx,
        matx,
        itx
    }

    public enum StorageBusInterface
    {
        sata,
        nvme
    }

    public enum DramType
    {
        DDR3,
        DDR4,
        DDR5
    }
}
