using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Scenario
{
    public string scenarioName;
    public string scenarioTitle;
    public string scenarioDescription;
    public bool isTutorialScenario;
    public List<Objective> scenarioObjectives;
    public List<InventoryPart> inventoryParts;
    public string scenarioUUID;

}
