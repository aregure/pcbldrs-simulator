using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class CustomScenarioEditor : MonoBehaviour
{
    public InputField scenarioName;
    public InputField scenarioTitle;
    public InputField scenarioDesc;
    public Scenario scenario;
    void CreateEditor ()
    {
        // TODO create editor object
    }

    public void LoadScenario ()
    {
        string folder = Application.persistentDataPath + "/LevelData/";
        string levelFile = "";
        if (scenarioName.text == "")
            levelFile = "newLevel.json";
        else
            levelFile = scenarioName.text + ".json";

        string path = Path.Combine(folder, levelFile);

        if (File.Exists(path))
        {
            // TODO read data from JSON and transfer to Scenario scriptable object
            // TODO build scenario environment based on Scenario data
        } else
        {
            // TODO handle error where file specified does not exist
        }
    }

    public void CreateScenario ()
    {
        /*
        string folder = Application.persistentDataPath + "/LevelData/";
        string levelFile = "";
        if (levelNameLoad.text == "")
            levelFile = "newLevel.json";
        else
            levelFile = levelNameLoad.text + ".json";

        string path = Path.Combine(folder, levelFile);

        if (File.Exists(path))
        {
            // TODO error handling on saving with filename that already exists
        } else
        {
            // TODO handle file creation
        }
        */

        // string folder = Application.persistentDataPath + "/ScenarioData/";
        // string scenarioFile = "";
    }
}
