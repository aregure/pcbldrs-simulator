using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

// script keeps track of inventory items displayed, must sync with actual PlayerInventory
public class InventoryDisplay : MonoBehaviour
{
    public GameObject gameManager;
    public GameManagerController gc;
    public List<InventoryPart> items;
    public GameObject iconContainer;

    void Start()
    {
        gameManager = GameObject.Find("GameManager");
        gc = gameManager.GetComponent<GameManagerController>();
        items = gc.currentScenario.inventoryParts;
        iconContainer = Resources.Load<GameObject>("UIPrefabs/IconContainer");
        CreateDisplay();
    }

    public void CreateDisplay()
    {
        print("creating display");
        foreach (InventoryPart item in items)
        {
            if (item.isStowed)
            {
                var obj = Instantiate(iconContainer, transform);
                obj.GetComponentInChildren<RawImage>().texture = Resources.Load<Texture2D>($"Icons/{item.iconId}_wt");
                obj.GetComponent<IconController>().partId = item.partId;
                obj.GetComponentInChildren<TextMeshProUGUI>().text = item.partName;
            }
        }
    }

    public void UpdateDisplay()
    {
        if (transform.childCount > 0) 
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }
        CreateDisplay();
    }
}
