using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// acts as item database for every item in the current simulation
[CreateAssetMenu(menuName = "Inventory System / Inventory")]
public class InventoryObject : ScriptableObject
{
    private void Awake()
    {
        // GenerateInventory();
    }

    public List<InventoryPart> Container = new();

    public void GenerateInventory ()
    {
        // generate inventory here via JSON or some shit
    }

    public InventoryPart CheckForItem(string partId)
    {
        return Container.Find(item => item.partId == partId);
    }

}