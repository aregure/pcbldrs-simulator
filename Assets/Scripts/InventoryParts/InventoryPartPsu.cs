using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/Power Supply")]
public class InventoryPartPsu : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.PSU;
        prefabId = "psu";
    }

    public int maxPwr;
}
