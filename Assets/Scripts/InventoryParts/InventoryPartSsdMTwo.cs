using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/Solid State Drive (M.2)")]
public class InventoryPartSsdMTwo : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.SSDM2;
        busInterface = GameEnums.StorageBusInterface.sata;
        prefabId = "ssdm2";
    }

    public int capacity;
    public GameEnums.StorageBusInterface busInterface;
}
