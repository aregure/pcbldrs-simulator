using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/Motherboard")]
public class InventoryPartMotherboard : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.Motherboard;
        prefabId = "mobo_b450_matx";
    }

    public GameEnums.SocketType socketType;
    public string chipset;
    public List<Port> ports;
    public int maxMemoryCapacity;
}
