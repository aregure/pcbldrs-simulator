using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/Hard Drive")]
public class InventoryPartHdd : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.HDD;
        busInterface = GameEnums.StorageBusInterface.sata;
        prefabId = "hdd";
    }

    public int capacity;
    public GameEnums.StorageBusInterface busInterface;
}
