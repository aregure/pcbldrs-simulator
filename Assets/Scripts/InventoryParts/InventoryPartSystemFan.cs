using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/System Fan")]
public class InventoryPartSystemFan : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.SystemFan;
        prefabId = "systemFan_black";
    }
    public int maxFanSpeed;
}
