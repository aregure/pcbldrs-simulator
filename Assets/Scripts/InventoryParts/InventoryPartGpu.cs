using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/GPU")]
public class InventoryPartGpu : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.GPU;
        prefabId = "gpu";
    }

    public int minFreq;
    public int baseFreq;
    public int boostFreq;
    public int memFreq;
    public int perfFactor;
    public int maxPwrDraw;
    public List<Port> ports;
}
