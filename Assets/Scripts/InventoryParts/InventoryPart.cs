using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InventoryPartType
{
    CPU,
    CPUCooler,
    GPU,
    Motherboard,
    RAM,
    SSDM2,
    SSDSata,
    HDD,
    PSU,
    SystemFan,
    Case
}

[System.Serializable]
public abstract class InventoryPart : ScriptableObject
{
    public string partId;
    public string partName;
    public InventoryPartType partType;
    public GameEnums.DamageType damageType;
    public string iconId;
    public string prefabId;
    public bool isEnabled;
    public bool isStowed;

    public T CreateCopy<T>() where T: InventoryPart, new()
    {
        var copy = CreateInstance<T>();
        string serialized = JsonUtility.ToJson(this);
        JsonUtility.FromJsonOverwrite(serialized, copy);
        return copy;
    }
}
