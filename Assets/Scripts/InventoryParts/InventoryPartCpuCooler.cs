using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/CPU Cooler")]
public class InventoryPartCpuCooler : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.CPUCooler;
        prefabId = "cooler_bmd";
    }

    public int maxTdp;
    public int maxFanSpeed;
    public List<GameEnums.SocketType> supportedSockets;
}
