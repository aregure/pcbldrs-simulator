using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/Case")]
public class InventoryPartCase : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.Case;
        prefabId = "case_matx";
    }

    public GameEnums.CaseSpec caseSpec;
    public List<Port> attachList;
}
