using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/Solid State Drive (SATA)")]
public class InventoryPartSsdSata : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.SSDSata;
        busInterface = GameEnums.StorageBusInterface.sata;
        prefabId = "ssdSata";
    }

    public int capacity;
    public GameEnums.StorageBusInterface busInterface;
}
