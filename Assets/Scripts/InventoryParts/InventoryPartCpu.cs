using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/CPU")]
public class InventoryPartCpu : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.CPU;
        prefabId = "cpu_bmd";
    }

    public int minFreq;
    public int baseFreq;
    public int boostFreq;
    public int cores;
    public bool isMultiThreaded;
    public bool hasIgpu;
    public GameEnums.SocketType socketType;
    public int tdp;
    public int ihsSize;
    public int perfFactor;
    public int maxPwrDraw;
}
