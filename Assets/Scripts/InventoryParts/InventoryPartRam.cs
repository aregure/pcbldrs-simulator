using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Inventory Part/RAM")]
public class InventoryPartRam : InventoryPart
{
    private void Awake()
    {
        partType = InventoryPartType.RAM;
        prefabId = "ram_ddr4";
    }

    public int size;
    public int baseFreq;
    public List<int> xmpFreqs;
    public GameEnums.DramType dramType;
}
